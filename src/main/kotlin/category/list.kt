package xyz.kemonomachi.mokuroku.category.list

import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ListItem
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.ViewModel as AndroidViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.composable
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder

import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.R
import xyz.kemonomachi.mokuroku.creditable.list.ROUTE as CREDITABLE_LIST_ROUTE
import xyz.kemonomachi.mokuroku.work.list.ROUTE as WORK_LIST_ROUTE
import xyz.kemonomachi.mokuroku.ui.components.UIList
import xyz.kemonomachi.mokuroku.ui.components.NavCard
import xyz.kemonomachi.mokuroku.ui.components.Scaffold
import xyz.kemonomachi.mokuroku.ui.components.ScanFAB

internal const val ROUTE = "category/list"

internal fun NavGraphBuilder.destination(navCon: NavController) =
  composable(ROUTE) { Screen(
    toWorkList = { navCon.navigate(WORK_LIST_ROUTE) },
    toCreditableList = { navCon.navigate(CREDITABLE_LIST_ROUTE) },
    floatingActionButton = { ScanFAB(navCon) }
  ) }

@Composable
@OptIn(ExperimentalMaterialApi::class)
private fun Screen(
  toWorkList: () -> Unit,
  toCreditableList: () -> Unit,
  floatingActionButton: @Composable () -> Unit
) =
  Scaffold(
    floatingActionButton = floatingActionButton,
    title = stringResource(R.string.category)
  ) {
    val viewModel: ViewModel = viewModel()
    val workCount by viewModel.workCount.observeAsState()
    val creditableCount by viewModel.creditableCount.observeAsState()

    workCount?.let { work ->
      creditableCount?.let { creditable ->
        UIList(listOf(
          Triple(R.string.work, work, toWorkList),
          Triple(R.string.creditable, creditable, toCreditableList)
        )) { (titleId, count, navigation) ->
          NavCard(navigation) {
            ListItem(
              text = { Text(stringResource(titleId)) },
              trailing = { Text(count.toString()) }
            )
          }
        }
      }
    }
  }

internal class ViewModel : AndroidViewModel() {
  val workCount = AppDatabase.workDAO().count()
  val creditableCount = AppDatabase.creditableDAO().count()
}
