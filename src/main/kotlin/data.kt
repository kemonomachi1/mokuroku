package xyz.kemonomachi.mokuroku.data

import java.time.LocalDate

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.room.Dao
import androidx.room.Database
import androidx.room.Insert
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import androidx.room.Update

import xyz.kemonomachi.mokuroku.MIGRATION_1_2
import xyz.kemonomachi.mokuroku.MIGRATION_2_3
import xyz.kemonomachi.mokuroku.MIGRATION_3_4
import xyz.kemonomachi.mokuroku.MIGRATION_4_5
import xyz.kemonomachi.mokuroku.MIGRATION_5_6
import xyz.kemonomachi.mokuroku.MIGRATION_6_7
import xyz.kemonomachi.mokuroku.MIGRATION_7_8
import xyz.kemonomachi.mokuroku.MIGRATION_8_9
import xyz.kemonomachi.mokuroku.MIGRATION_9_10
import xyz.kemonomachi.mokuroku.MIGRATION_10_11
import xyz.kemonomachi.mokuroku.MIGRATION_11_12

import xyz.kemonomachi.mokuroku.base.Identifiable
import xyz.kemonomachi.mokuroku.creditable.DAO as CreditableDAO
import xyz.kemonomachi.mokuroku.junction.MangaVolumeAuthorDAO
import xyz.kemonomachi.mokuroku.junction.NovelVolumeAuthorDAO
import xyz.kemonomachi.mokuroku.manga.series.DAO as MangaSeriesDAO
import xyz.kemonomachi.mokuroku.manga.volume.DAO as MangaVolumeDAO
import xyz.kemonomachi.mokuroku.manga.volume.variant.DAO as MangaVolumeVariantDAO
import xyz.kemonomachi.mokuroku.novel.series.DAO as NovelSeriesDAO
import xyz.kemonomachi.mokuroku.novel.volume.DAO as NovelVolumeDAO
import xyz.kemonomachi.mokuroku.novel.volume.variant.DAO as NovelVolumeVariantDAO
import xyz.kemonomachi.mokuroku.work.DAO as WorkDAO

internal fun <S, T> MediatorLiveData<T>.observeFirstValue(
  source: LiveData<S>,
  map: (S) -> T
) = addSource(source) {
  setValue(map(it))
  removeSource(source)
}

internal data class ISBN(val value: String) {
  override fun toString() = value
}

internal class Converters {
  @TypeConverter fun fromISBN(isbn: ISBN) = isbn.value
  @TypeConverter fun toISBN(str: String) = ISBN(str)
  @TypeConverter fun fromLocalDate(date: LocalDate?) = date?.toString()
  @TypeConverter fun toLocalDate(str: String?) = str?.let { LocalDate.parse(it) }
}

@Dao
internal abstract class DAO<T: Identifiable> {
  @Insert
  abstract suspend fun insert(entity: T): Long

  @Update abstract suspend fun update(entity: T)

  open suspend fun ensure(entity: T) =
    if(entity.id == 0L) insert(entity)
    else entity.id
}

@Database(
  entities = [
    xyz.kemonomachi.mokuroku.manga.volume.Entity::class,
    xyz.kemonomachi.mokuroku.manga.volume.variant.Entity::class,
    xyz.kemonomachi.mokuroku.manga.series.Entity::class,
    xyz.kemonomachi.mokuroku.novel.volume.Entity::class,
    xyz.kemonomachi.mokuroku.novel.volume.variant.Entity::class,
    xyz.kemonomachi.mokuroku.novel.series.Entity::class,
    xyz.kemonomachi.mokuroku.work.Entity::class,
    xyz.kemonomachi.mokuroku.creditable.Entity::class,
    xyz.kemonomachi.mokuroku.junction.MangaVolumeAuthor::class,
    xyz.kemonomachi.mokuroku.junction.NovelVolumeAuthor::class
  ],
  views = [
    xyz.kemonomachi.mokuroku.manga.series.list.Element::class,
    xyz.kemonomachi.mokuroku.novel.series.list.Element::class
    ],
  version = 12
)
@TypeConverters(Converters::class)
internal abstract class AppDatabase : RoomDatabase() {
  protected abstract fun mangaVolumeDAO(): MangaVolumeDAO
  protected abstract fun mangaVolumeVariantDAO(): MangaVolumeVariantDAO
  protected abstract fun mangaSeriesDAO(): MangaSeriesDAO
  protected abstract fun novelSeriesDAO(): NovelSeriesDAO
  protected abstract fun novelVolumeDAO(): NovelVolumeDAO
  protected abstract fun novelVolumeVariantDAO(): NovelVolumeVariantDAO
  protected abstract fun workDAO(): WorkDAO
  protected abstract fun creditableDAO(): CreditableDAO
  protected abstract fun mangaVolumeAuthorDAO(): MangaVolumeAuthorDAO
  protected abstract fun novelVolumeAuthorDAO(): NovelVolumeAuthorDAO

  companion object {
    protected lateinit var instance: AppDatabase

    fun mangaVolumeDAO() = instance.mangaVolumeDAO()
    fun mangaVolumeVariantDAO() = instance.mangaVolumeVariantDAO()
    fun mangaSeriesDAO() = instance.mangaSeriesDAO()
    fun novelSeriesDAO() = instance.novelSeriesDAO()
    fun novelVolumeDAO() = instance.novelVolumeDAO()
    fun novelVolumeVariantDAO() = instance.novelVolumeVariantDAO()
    fun workDAO() = instance.workDAO()
    fun creditableDAO() = instance.creditableDAO()
    fun mangaVolumeAuthorDAO() = instance.mangaVolumeAuthorDAO()
    fun novelVolumeAuthorDAO() = instance.novelVolumeAuthorDAO()

    fun initialize(context: Context) =
      Room.databaseBuilder(context, AppDatabase::class.java, "mokuroku_database")
        .addMigrations(
          MIGRATION_1_2,
          MIGRATION_2_3,
          MIGRATION_3_4,
          MIGRATION_4_5,
          MIGRATION_5_6,
          MIGRATION_6_7,
          MIGRATION_7_8,
          MIGRATION_8_9,
          MIGRATION_9_10,
          MIGRATION_10_11,
          MIGRATION_11_12
        ).build()
        .let { instance = it }
  }
}
