package xyz.kemonomachi.mokuroku

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController

import xyz.kemonomachi.mokuroku.category.list.destination as categoryList
import xyz.kemonomachi.mokuroku.category.list.ROUTE as CATEGORY_LIST_ROUTE
import xyz.kemonomachi.mokuroku.creditable.details.destination as creditableDetails
import xyz.kemonomachi.mokuroku.creditable.edit.destination as creditableEdit
import xyz.kemonomachi.mokuroku.creditable.list.destination as creditableList
import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.entity_selection.destination as entitySelection
import xyz.kemonomachi.mokuroku.manga.series.details.destination as mangaSeriesDetails
import xyz.kemonomachi.mokuroku.manga.series.edit.destination as mangaSeriesEdit
import xyz.kemonomachi.mokuroku.manga.volume.create.destination as mangaVolumeCreate
import xyz.kemonomachi.mokuroku.manga.volume.details.destination as mangaVolumeDetails
import xyz.kemonomachi.mokuroku.manga.volume.edit.destination as mangaVolumeEdit
import xyz.kemonomachi.mokuroku.manga.volume.variant.edit.destination as mangaVolumeVariantEdit
import xyz.kemonomachi.mokuroku.novel.series.details.destination as novelSeriesDetails
import xyz.kemonomachi.mokuroku.novel.series.edit.destination as novelSeriesEdit
import xyz.kemonomachi.mokuroku.novel.volume.create.destination as novelVolumeCreate
import xyz.kemonomachi.mokuroku.novel.volume.details.destination as novelVolumeDetails
import xyz.kemonomachi.mokuroku.novel.volume.edit.destination as novelVolumeEdit
import xyz.kemonomachi.mokuroku.novel.volume.variant.edit.destination as novelVolumeVariantEdit
import xyz.kemonomachi.mokuroku.work.details.destination as workDetails
import xyz.kemonomachi.mokuroku.work.edit.destination as workEdit
import xyz.kemonomachi.mokuroku.work.list.destination as workList

internal class MainActivity : ComponentActivity() {
  protected override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    AppDatabase.initialize(applicationContext)

    setContent {
      val navCon = rememberNavController()

      NavHost(navCon, startDestination = CATEGORY_LIST_ROUTE) {
        categoryList(navCon)
        entitySelection(navCon)
        creditableList(navCon)
        creditableDetails(navCon)
        creditableEdit(navCon)
        workList(navCon)
        workDetails(navCon)
        workEdit(navCon)
        mangaSeriesDetails(navCon)
        mangaSeriesEdit(navCon)
        mangaVolumeCreate(navCon)
        mangaVolumeDetails(navCon)
        mangaVolumeEdit(navCon)
        mangaVolumeVariantEdit(navCon)
        novelSeriesDetails(navCon)
        novelSeriesEdit(navCon)
        novelVolumeCreate(navCon)
        novelVolumeDetails(navCon)
        novelVolumeEdit(navCon)
        novelVolumeVariantEdit(navCon)
      }
    }
  }
}
