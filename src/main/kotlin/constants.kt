package xyz.kemonomachi.mokuroku

internal object C {
  const val id = "id"
  const val isbn = "isbn"
  const val work = "work"
  const val series = "series"
  const val volume = "volume"
}
