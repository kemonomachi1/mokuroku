package xyz.kemonomachi.mokuroku.entity_selection

import android.os.Bundle
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.navigation.compose.navArgument
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType

import xyz.kemonomachi.mokuroku.base.composable
import xyz.kemonomachi.mokuroku.C
import xyz.kemonomachi.mokuroku.data.ISBN
import xyz.kemonomachi.mokuroku.R
import xyz.kemonomachi.mokuroku.manga.volume.create.fromScratchRoute as mangaVolumeCreateRoute
import xyz.kemonomachi.mokuroku.manga.volume.create.inWorkRoute as mangaVolumeCreateInWorkRoute
import xyz.kemonomachi.mokuroku.novel.volume.create.fromScratchRoute as novelVolumeCreateRoute
import xyz.kemonomachi.mokuroku.novel.volume.create.inWorkRoute as novelVolumeCreateInWorkRoute
import xyz.kemonomachi.mokuroku.ui.components.UIList
import xyz.kemonomachi.mokuroku.ui.components.Scaffold
import xyz.kemonomachi.mokuroku.ui.components.UpButton
import xyz.kemonomachi.mokuroku.base.volume.create.getISBN

private object Args {
  val isbn = navArgument(C.isbn) { type = NavType.StringType }
  val work = navArgument(C.work) { type = NavType.LongType }
}

private fun Bundle.getISBN() = ISBN(getString(Args.isbn.name)!!)
private fun Bundle.getWorkId() = getLong(Args.work.name)

private const val BASE_ROUTE = "entity_selection"
internal val FROM_SCRATCH_ROUTE = "$BASE_ROUTE/{${Args.isbn.name}}"
internal val IN_WORK_ROUTE = "$BASE_ROUTE/{${Args.isbn.name}}/{${Args.work.name}}"

internal fun fromScratchRoute(isbn: ISBN) = "$BASE_ROUTE/$isbn"
internal fun inWorkRoute(isbn: ISBN, work: Long) = "$BASE_ROUTE/$isbn/$work"

private fun NavGraphBuilder.fromScratch(navCon: NavController) =
  composable(FROM_SCRATCH_ROUTE, Args.isbn) {
    val isbn = it.getISBN()

    Screen(
      toMangaVolumeEdit = { navCon.navigate(mangaVolumeCreateRoute(isbn)) },
      toNovelVolumeEdit = { navCon.navigate(novelVolumeCreateRoute(isbn)) },
      navigationIcon = { UpButton(navCon) }
    )
  }

private fun NavGraphBuilder.inWork(navCon: NavController) =
  composable(IN_WORK_ROUTE, Args.isbn, Args.work) {
    val isbn = it.getISBN()
    val workId = it.getWorkId()

    Screen(
      toMangaVolumeEdit =
        { navCon.navigate(mangaVolumeCreateInWorkRoute(isbn, workId)) },
      toNovelVolumeEdit =
        { navCon.navigate(novelVolumeCreateInWorkRoute(isbn, workId)) },
      navigationIcon = { UpButton(navCon) }
    )
  }

internal fun NavGraphBuilder.destination(navCon: NavController) {
  fromScratch(navCon)
  inWork(navCon)
}

@Composable
private fun Screen(
  toMangaVolumeEdit: () -> Unit,
  toNovelVolumeEdit: () -> Unit,
  navigationIcon: @Composable () -> Unit
) =
  Scaffold(
    title = stringResource(R.string.register_new),
    navigationIcon = navigationIcon
  ) {
    UIList(listOf(
      R.string.manga to toMangaVolumeEdit,
      R.string.novel to toNovelVolumeEdit
    )) { (labelId, navigate) ->
      Button(onClick = navigate) {
        Text(stringResource(labelId))
      }
    }
  }
