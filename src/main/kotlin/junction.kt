package xyz.kemonomachi.mokuroku.junction

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Insert
import androidx.room.OnConflictStrategy

import xyz.kemonomachi.mokuroku.creditable.Entity as CreditableEntity
import xyz.kemonomachi.mokuroku.data.AppDatabase

@Entity(
  tableName = "manga_volume_authors",
  primaryKeys = ["manga_volume_id", "author_id"],
  foreignKeys = [
    ForeignKey(
      entity = xyz.kemonomachi.mokuroku.manga.volume.Entity::class,
      parentColumns = ["id"],
      childColumns = ["manga_volume_id"]
    ),
    ForeignKey(
      entity = CreditableEntity::class,
      parentColumns = ["id"],
      childColumns = ["author_id"]
    )
  ]
)
internal data class MangaVolumeAuthor(
  @ColumnInfo(name = "manga_volume_id", index = true)
  val mangaVolumeId: Long,

  @ColumnInfo(name = "author_id", index = true)
  val authorId: Long
)

@Dao
internal abstract class MangaVolumeAuthorDAO {
  @Insert(onConflict = OnConflictStrategy.IGNORE)
  abstract suspend fun insert(entities: List<MangaVolumeAuthor>)

  @Delete
  abstract suspend fun delete(entities: List<MangaVolumeAuthor>)

  suspend fun insert(volumeId: Long, authors: List<CreditableEntity>) =
    insert(authors.map {
      MangaVolumeAuthor(volumeId, AppDatabase.creditableDAO().ensure(it))
    })

  suspend fun delete(volumeId: Long, authors: List<CreditableEntity>) =
    delete(authors.map { MangaVolumeAuthor(volumeId, it.id) })
}

@Entity(
  tableName = "novel_volume_authors",
  primaryKeys = ["novel_volume_id", "author_id"],
  foreignKeys = [
    ForeignKey(
      entity = xyz.kemonomachi.mokuroku.novel.volume.Entity::class,
      parentColumns = ["id"],
      childColumns = ["novel_volume_id"]
    ),
    ForeignKey(
      entity = xyz.kemonomachi.mokuroku.creditable.Entity::class,
      parentColumns = ["id"],
      childColumns = ["author_id"]
    )
  ]
)
internal data class NovelVolumeAuthor(
  @ColumnInfo(name = "novel_volume_id", index = true)
  val novelVolumeId: Long,

  @ColumnInfo(name = "author_id", index = true)
  val authorId: Long
)

@Dao
internal abstract class NovelVolumeAuthorDAO {
  @Insert(onConflict = OnConflictStrategy.IGNORE)
  abstract suspend fun insert(entities: List<NovelVolumeAuthor>)

  @Delete
  abstract suspend fun delete(entities: List<NovelVolumeAuthor>)

  suspend fun insert(volumeId: Long, authors: List<CreditableEntity>) =
    insert(authors.map {
      NovelVolumeAuthor(volumeId, AppDatabase.creditableDAO().ensure(it))
    })

  suspend fun delete(volumeId: Long, authors: List<CreditableEntity>) =
    delete(authors.map { NovelVolumeAuthor(volumeId, it.id) })
}
