package xyz.kemonomachi.mokuroku.ui.components

import java.util.UUID

import kotlinx.coroutines.launch

import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.ActivityResultRegistryOwner
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Card
import androidx.compose.material.darkColors
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ExtendedFloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.ListItem
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold as MaterialScaffold
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.TextField
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController

import xyz.kemonomachi.mokuroku.base.Displayable
import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.contract.ScanBarcode
import xyz.kemonomachi.mokuroku.creditable.Entity as CreditableEntity
import xyz.kemonomachi.mokuroku.data.ISBN
import xyz.kemonomachi.mokuroku.entity_selection.fromScratchRoute as entitySelectionRoute
import xyz.kemonomachi.mokuroku.manga.volume.details.route as mangaVolumeDetailsRoute
import xyz.kemonomachi.mokuroku.novel.volume.details.route as novelVolumeDetailsRoute
import xyz.kemonomachi.mokuroku.R

@Composable
internal fun Scaffold(
  floatingActionButton: @Composable () -> Unit = {},
  title: String,
  navigationIcon: (@Composable () -> Unit)? = null,
  actions: @Composable RowScope.() -> Unit = {},
  bodyContent: @Composable () -> Unit
) =
  MaterialTheme(colors = darkColors()) {
    MaterialScaffold(
      floatingActionButton = floatingActionButton,
      topBar = {
        TopAppBar(
          title = { Text(title) },
          navigationIcon = navigationIcon,
          actions = actions
        )
      }
    ) {
      Column {
        bodyContent()
      }
    }
  }

@Composable
internal fun ScanFAB(
  navCon: NavController,
  createEntityRoute: (ISBN) -> String = ::entitySelectionRoute
) =
  ScanFAB(
    toCreateEntity = { navCon.navigate(createEntityRoute(it)) },
    toMangaVolume = { navCon.navigate(mangaVolumeDetailsRoute(it)) },
    toNovelVolume = { navCon.navigate(novelVolumeDetailsRoute(it)) }
  )

@Composable
private fun ScanFAB(
  toCreateEntity: (ISBN) -> Unit,
  toMangaVolume: (Long) -> Unit,
  toNovelVolume: (Long) -> Unit
) {
  val activityResultRegistry =
    (LocalContext.current as ActivityResultRegistryOwner).activityResultRegistry
  val coroutineScope = rememberCoroutineScope()

  var launcher by remember { mutableStateOf<ActivityResultLauncher<Unit>?>(null) }

  DisposableEffect(true) {
    val key = "ScanFAB: ${UUID.randomUUID()}"

    launcher = activityResultRegistry.register(key, ScanBarcode()) {
      it?.let { isbn ->
        coroutineScope.launch {
          searchAndNavigate(isbn, toMangaVolume, toNovelVolume) ?: toCreateEntity(isbn)
        }
      }
    }

    onDispose {
      launcher?.unregister()
    }
  }

  launcher?.apply {
    ExtendedFloatingActionButton(
      text = { Text(stringResource(R.string.scan)) },
      onClick = { launch(Unit) }
    )
  }
}

private suspend fun searchAndNavigate(
  isbn: ISBN,
  toMangaVolume: (Long) -> Unit,
  toNovelVolume: (Long) -> Unit
) =
  AppDatabase.mangaVolumeDAO().byISBN(isbn)?.let {
    toMangaVolume(it.volume.id)
  } ?: AppDatabase.novelVolumeDAO().byISBN(isbn)?.let {
    toNovelVolume(it.volume.id)
  }

@Composable
@OptIn(ExperimentalMaterialApi::class)
internal fun NavCard(
  navigation: () -> Unit,
  content: @Composable () -> Unit
) =
  Card(onClick = navigation, content = content)

@Composable
@OptIn(ExperimentalMaterialApi::class)
internal fun CreditableCard(
  creditable: CreditableEntity,
  toCreditableDetails: (Long) -> Unit
) =
  NavCard({ toCreditableDetails(creditable.id) }) {
    ListItem(
      text = { Text(creditable.name) }
    )
  }

@Composable
internal fun <T> LabeledList(
  titleId: Int,
  items: List<T>,
  cardEmitter: @Composable (T) -> Unit
) =
  items.ifAny {
    Column {
      Text(stringResource(titleId))

      InternalList(it, cardEmitter)
    }
  }

@Composable
internal fun <T> UIList(
  items: List<T>,
  cardEmitter: @Composable (T) -> Unit
) =
  items.ifAny { InternalList(it, cardEmitter) }

@Composable
private fun <T> InternalList(
  elements: List<T>,
  cardEmitter: @Composable (T) -> Unit
) =
  LazyColumn {
    items(elements) { cardEmitter(it) }
  }

inline private fun <E> List<E>.ifAny(block: (List<E>) -> Unit) {
  if(isNotEmpty()) let(block)
}

@Composable
internal fun DictItem(labelId: Int, content: Any) = 
  Row {
    Text(stringResource(labelId))
    Text(content.toString())
  }

@Composable
internal fun NextTextField(
  value: String,
  onValueChange: (String) -> Unit,
  labelId: Int,
  focusRequester: FocusRequester? = null
) =
  TextField(
    value = value,
    onValueChange = onValueChange,
    label = { Text(stringResource(labelId)) },
    keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next),
    keyboardActions = KeyboardActions(onNext = { focusRequester?.requestFocus() })
  )

@Composable
internal fun DoneTextField(
  value: String,
  onValueChange: (String) -> Unit,
  labelId: Int,
  modifier: Modifier = Modifier,
  keyboardType: KeyboardType = KeyboardType.Text,
  onDone: () -> Unit
) =
  TextField(
    value = value,
    onValueChange = onValueChange,
    label = { Text(stringResource(labelId)) },
    keyboardOptions = KeyboardOptions(
      imeAction = ImeAction.Done,
      keyboardType = keyboardType
    ),
    keyboardActions = KeyboardActions(onDone = { onDone() }),
    modifier = modifier
  )

@Composable
internal fun <E: Displayable> DropDown(
  elements: List<E>,
  selectedValue: E?,
  createElement: (String) -> E,
  onSelection: (E) -> Unit,
  enabled: Boolean = true
) {
  var expanded by remember { mutableStateOf(false) }
  var filterText by remember { mutableStateOf("") }

  Box {
    TextButton(
      onClick = { expanded = enabled },
      modifier = Modifier.padding(vertical = 10.dp)
    ) {
      Text(
        selectedValue?.let { it.text } ?: stringResource(R.string.no_selection)
      )
    }

    DropdownMenu(
      expanded = expanded,
      onDismissRequest = { expanded = false },
    ) {
      TextField(
        value = filterText,
        onValueChange = { filterText = it },
        label = { Text(stringResource(R.string.search)) }
      )

      elements.filter {
        it.text.startsWith(filterText)
      }
      .let { when {
        it.isEmpty() ->
          TextButton(
            onClick = {
              onSelection(createElement(filterText))
              expanded = false
            },
            modifier = Modifier.padding(vertical = 10.dp)
          ) {
            Text(stringResource(R.string.register_new))
          }
        else ->
          it.forEach {
            DropdownMenuItem(
              onClick = {
                onSelection(it)
                expanded = false
              }
            ) {
              Text(it.text)
            }
          }
      }}
    }
  }
}

@Composable
internal fun UpButton(navCon: NavController) =
  IconButton(onClick = { navCon.navigateUp() }) {
    Icon(Icons.Filled.ArrowBack, stringResource(R.string.up_button))
  }

@Composable
internal fun DoneButton(onClick: () -> Unit) =
  IconButton(onClick = onClick) {
    Icon(Icons.Filled.Done, stringResource(R.string.done_action))
  }

@Composable
internal fun EditButton(navigation: () -> Unit) =
  NavAction(Icons.Filled.Edit, R.string.edit_action, navigation)

@Composable
internal fun NavAction(
  icon: ImageVector,
  contentDescriptionId: Int,
  navigation: () -> Unit
) =
  IconButton(onClick = navigation) {
    Icon(icon, stringResource(contentDescriptionId))
  }
