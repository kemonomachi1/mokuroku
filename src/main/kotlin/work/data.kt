package xyz.kemonomachi.mokuroku.work

import kotlin.collections.List

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Entity as RoomEntity
import androidx.room.PrimaryKey
import androidx.room.Ignore
import androidx.room.Query
import androidx.room.Transaction

import xyz.kemonomachi.mokuroku.base.Displayable
import xyz.kemonomachi.mokuroku.base.Identifiable
import xyz.kemonomachi.mokuroku.data.DAO as BaseDAO
import xyz.kemonomachi.mokuroku.work.details.Data as DetailsData
import xyz.kemonomachi.mokuroku.work.list.Element as ListElement

@RoomEntity(tableName = "works")
internal data class Entity (
  @PrimaryKey(autoGenerate = true) override val id: Long = 0,
  val title: String
) : Identifiable, Displayable {
  @delegate:Ignore override val text by this::title
}

@Dao
internal abstract class DAO : BaseDAO<Entity>() {
  @Query("select id,title from works")
  abstract fun all(): LiveData<List<Entity>>

  @Query("select count(*) from works")
  abstract fun count(): LiveData<Int>

  @Query("""
    select
      works.id,
      works.title,
      count(manga_series.id) as seriesCount
    from works
    left join manga_series
    on works.id = manga_series.work_id
    group by works.id
  """)
  abstract fun list(): LiveData<List<ListElement>>

  @Query("""
    select id,title
    from works
    where id = :id
  """)
  abstract fun edit(id: Long): LiveData<Entity>

  @Transaction
  @Query("""
    select id,title
    from works
    where id = :id
  """)
  abstract fun details(id: Long): LiveData<DetailsData>

  suspend fun update(id: Long, title: String) =
    update(Entity(id, title))
}
