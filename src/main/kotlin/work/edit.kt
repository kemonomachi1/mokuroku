package xyz.kemonomachi.mokuroku.work.edit

import android.os.Bundle
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel as AndroidViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.navArgument
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType

import xyz.kemonomachi.mokuroku.base.composable
import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.C
import xyz.kemonomachi.mokuroku.R
import xyz.kemonomachi.mokuroku.ui.components.DoneButton
import xyz.kemonomachi.mokuroku.ui.components.DoneTextField
import xyz.kemonomachi.mokuroku.ui.components.Scaffold
import xyz.kemonomachi.mokuroku.ui.components.UpButton

private object Args {
  val id = navArgument(C.id) { type = NavType.LongType }
}

private fun Bundle.getId() = getLong(Args.id.name)

private const val BASE_ROUTE = "work/edit"
private val ROUTE = "$BASE_ROUTE/{${Args.id.name}}"

internal fun route(id: Long) = "$BASE_ROUTE/$id"

internal fun NavGraphBuilder.destination(navCon: NavController) =
  composable(ROUTE, Args.id) {
    Screen(
      id = it.getId(),
      navigationIcon = { UpButton(navCon) },
      exit = navCon::popBackStack
    )
  }

@Composable
private fun Screen(
  id: Long,
  navigationIcon: @Composable () -> Unit,
  exit: () -> Unit
) {
  val viewModel: ViewModel = viewModel(factory = ViewModelFactory(id, exit))

  Scaffold(
    title = stringResource(R.string.work),
    navigationIcon = navigationIcon,
    actions = {
      DoneButton {
        viewModel.saveAndExit()
      }
    }
  ) {
    val title by viewModel.title.observeAsState(initial = "")

    DoneTextField(
      value = title,
      onValueChange = { viewModel.title.setValue(it) },
      labelId = R.string.title,
      onDone = {
        viewModel.saveAndExit()
      }
    )
  }
}

private class ViewModelFactory(
  private val id: Long,
  private val exit: () -> Unit
) : ViewModelProvider.Factory {

  @Suppress("UNCHECKED_CAST")
  override fun <T : AndroidViewModel> create(modelClass: Class<T>) =
    ViewModel(id, exit) as T
}

internal class ViewModel(
  private val id: Long,
  private val exit: () -> Unit
) : AndroidViewModel() {
  val title = MediatorLiveData<String>()

  init {
    title.addSource(AppDatabase.workDAO().edit(id)) {
      title.setValue(it.title)
    }
  }

  fun saveAndExit() =
    viewModelScope.launch(Dispatchers.IO) {
      AppDatabase.workDAO().update(
        id = id,
        title = title.value!!
      )

      withContext(Dispatchers.Main) { exit() }
    }
}
