package xyz.kemonomachi.mokuroku.work.list

import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ListItem
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.ViewModel as AndroidViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.composable
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.room.Embedded

import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.R
import xyz.kemonomachi.mokuroku.ui.components.UIList
import xyz.kemonomachi.mokuroku.ui.components.NavCard
import xyz.kemonomachi.mokuroku.ui.components.Scaffold
import xyz.kemonomachi.mokuroku.ui.components.ScanFAB
import xyz.kemonomachi.mokuroku.ui.components.UpButton
import xyz.kemonomachi.mokuroku.work.details.route as workDetailsRoute

internal const val ROUTE = "work/list"

internal fun NavGraphBuilder.destination(navCon: NavController) =
  composable(ROUTE) { Screen(
    toWorkDetails = { navCon.navigate(workDetailsRoute(it)) },
    floatingActionButton = { ScanFAB(navCon) },
    navigationIcon = { UpButton(navCon) }
  ) }

@Composable
@OptIn(ExperimentalMaterialApi::class)
private fun Screen(
  toWorkDetails: (Long) -> Unit,
  floatingActionButton: @Composable () -> Unit,
  navigationIcon: @Composable () -> Unit
) =
  Scaffold(
    floatingActionButton = floatingActionButton,
    title = stringResource(R.string.work),
    navigationIcon = navigationIcon,
  ) {
    val viewModel: ViewModel = viewModel()
    val works by viewModel.works.observeAsState(initial = emptyList())

    UIList(works) { with(it) {
      NavCard({ toWorkDetails(work.id) }) { ListItem(
        text = { Text(work.title) },
        trailing = { Text(seriesCount.toString()) }
      ) }
    } }
  }

internal class ViewModel : AndroidViewModel() {
  val works = AppDatabase.workDAO().list()
}

internal data class Element(
  @Embedded
  val work: xyz.kemonomachi.mokuroku.work.Entity,

  val seriesCount: Int
)
