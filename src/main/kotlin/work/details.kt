package xyz.kemonomachi.mokuroku.work.details

import kotlin.collections.List

import android.os.Bundle
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ListItem
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.lifecycle.ViewModel as AndroidViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.navArgument
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.room.Embedded
import androidx.room.Relation

import xyz.kemonomachi.mokuroku.base.composable
import xyz.kemonomachi.mokuroku.base.series.ListElement as SeriesListElement
import xyz.kemonomachi.mokuroku.C
import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.entity_selection.inWorkRoute as entitySelectionRoute
import xyz.kemonomachi.mokuroku.manga.series.details.route as mangaSeriesDetailsRoute
import xyz.kemonomachi.mokuroku.R
import xyz.kemonomachi.mokuroku.novel.series.details.route as novelSeriesDetailsRoute
import xyz.kemonomachi.mokuroku.ui.components.DictItem
import xyz.kemonomachi.mokuroku.ui.components.EditButton
import xyz.kemonomachi.mokuroku.ui.components.LabeledList
import xyz.kemonomachi.mokuroku.ui.components.NavCard
import xyz.kemonomachi.mokuroku.ui.components.Scaffold
import xyz.kemonomachi.mokuroku.ui.components.ScanFAB
import xyz.kemonomachi.mokuroku.ui.components.UpButton
import xyz.kemonomachi.mokuroku.work.edit.route as workEditRoute

private object Args {
  val id = navArgument(C.id) { type = NavType.LongType }
}

private fun Bundle.getId() = getLong(Args.id.name)

private const val BASE_ROUTE = "work/details"
private val ROUTE = "$BASE_ROUTE/{${Args.id.name}}"

internal fun route(id: Long) = "$BASE_ROUTE/$id"

internal fun NavGraphBuilder.destination(navCon: NavController) =
  composable(ROUTE, Args.id) {
    val id = it.getId()

    Screen(
      id = id,
      toMangaSeriesDetails = { navCon.navigate(mangaSeriesDetailsRoute(it)) },
      toNovelSeriesDetails = { navCon.navigate(novelSeriesDetailsRoute(it)) },
      toWorkEdit = { navCon.navigate(workEditRoute(id)) },
      floatingActionButton = { ScanFAB(navCon) { entitySelectionRoute(it, id) } },
      navigationIcon = { UpButton(navCon) }
    )
  }

internal data class Data(
  @Embedded val work: xyz.kemonomachi.mokuroku.work.Entity,

  @Relation(parentColumn = "id", entityColumn = "work_id")
  val mangaSeries: List<xyz.kemonomachi.mokuroku.manga.series.list.Element>,

  @Relation(parentColumn = "id", entityColumn = "work_id")
  val novelSeries: List<xyz.kemonomachi.mokuroku.novel.series.list.Element>
)

@Composable
@OptIn(ExperimentalMaterialApi::class)
private fun Screen(
  id: Long,
  toMangaSeriesDetails: (Long) -> Unit,
  toNovelSeriesDetails: (Long) -> Unit,
  toWorkEdit: () -> Unit,
  floatingActionButton: @Composable () -> Unit,
  navigationIcon: @Composable () -> Unit
) {
  val viewModel: ViewModel = viewModel(factory = ViewModelFactory(id))
  val state by viewModel.contents.observeAsState()

  state?.run {
    Scaffold(
      floatingActionButton = floatingActionButton,
      title = work.title,
      navigationIcon = navigationIcon,
      actions = { EditButton(toWorkEdit) }
    ) {
      DictItem(R.string.title, work.title)

      LabeledList(
        titleId = R.string.manga_series_list,
        items = mangaSeries
      ) { SeriesCard(toMangaSeriesDetails, it) }

      LabeledList(
        titleId = R.string.novel_series_list,
        items = novelSeries
      ) { SeriesCard(toNovelSeriesDetails, it) }
    }
  }
}

@Composable
@OptIn(ExperimentalMaterialApi::class)
private fun SeriesCard(
  toSeriesDetails: (Long) -> Unit,
  element: SeriesListElement
) =
  with(element) {
    NavCard({ toSeriesDetails(series.id) }) { ListItem(
      text = { Text(series.title) },
      trailing = { Text(volumeCount.toString()) }
    ) }
  }

private class ViewModelFactory(
  private val id: Long
) : ViewModelProvider.Factory {

  @Suppress("UNCHECKED_CAST")
  override fun <T : AndroidViewModel> create(modelClass: Class<T>) =
    ViewModel(id) as T
}

internal class ViewModel(val id: Long) : AndroidViewModel() {
  val contents = AppDatabase.workDAO().details(id)
}
