package xyz.kemonomachi.mokuroku.base.series

import androidx.lifecycle.LiveData

import xyz.kemonomachi.mokuroku.base.Series
import xyz.kemonomachi.mokuroku.base.series.edit.Data as EditData
import xyz.kemonomachi.mokuroku.creditable.Entity as CreditableEntity
import xyz.kemonomachi.mokuroku.data.DAO as BaseDAO
import xyz.kemonomachi.mokuroku.work.Entity as WorkEntity

internal abstract class DAO<SERIES: Series<SERIES>> : BaseDAO<SERIES>() {
  abstract fun byWork(workId: Long): LiveData<List<SERIES>>
  abstract fun edit(id: Long): LiveData<out EditData<SERIES>>
  abstract fun authors(id: Long): LiveData<List<CreditableEntity>>
  abstract suspend fun update(id: Long, title: String, work: WorkEntity)
}

internal interface ListElement {
  val series: Series<*>
  val volumeCount: Int
}

