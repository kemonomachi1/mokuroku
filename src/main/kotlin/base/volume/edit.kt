package xyz.kemonomachi.mokuroku.base.volume.edit

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

import android.os.Bundle
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.compose.navArgument
import androidx.navigation.NavType

import xyz.kemonomachi.mokuroku.base.Series
import xyz.kemonomachi.mokuroku.base.series.edit.Data as SeriesEditData
import xyz.kemonomachi.mokuroku.base.Volume
import xyz.kemonomachi.mokuroku.base.volume.create_edit.Repo
import xyz.kemonomachi.mokuroku.base.volume.create_edit.ViewModel as BaseViewModel
import xyz.kemonomachi.mokuroku.C
import xyz.kemonomachi.mokuroku.creditable.Entity as CreditableEntity
import xyz.kemonomachi.mokuroku.data.ISBN
import xyz.kemonomachi.mokuroku.data.observeFirstValue

internal interface Data<SERIES: Series<SERIES>> {
  val volume: Volume
  val series: SeriesEditData<SERIES>
  val authors: List<CreditableEntity>
}

internal object Args {
  val id = navArgument(C.id) { type = NavType.LongType }
}

internal fun Bundle.getId() = getLong(Args.id.name)

internal class ViewModel<VOLUME: Volume, SERIES: Series<SERIES>>(
  repo: Repo<VOLUME, SERIES>,
  private val id: Long,
  private val exit: () -> Unit
) : BaseViewModel<VOLUME, SERIES>(repo) {
  override val isbn = MediatorLiveData<ISBN?>()

  init {
    repo.volumeDAO.edit(id).let {
      isbn.observeFirstValue(it) { it.volume.isbn }
      title.observeFirstValue(it) { it.volume.title }
      volume.observeFirstValue(it) { it.volume.volume }
      publicationDate.observeFirstValue(it) { it.volume.publicationDate }
      authors.observeFirstValue(it) { it.authors }

      work.observeFirstValue(it) { it.series.work }
      series.observeFirstValue(it) { it.series.series }
    }
  }

  override fun saveAndExit() {
    viewModelScope.launch(Dispatchers.IO) {
      repo.volumeDAO.update(
        id = id,
        isbn = isbn.value!!,
        title = title.value!!,
        volume = volume.value!!,
        publicationDate = publicationDate.value!!,
        series = series.value!!,
        work = work.value!!,
        addedAuthors = addedAuthors.value!!,
        deletedAuthors = deletedAuthors.value!!
      )

      withContext(Dispatchers.Main) { exit() }
    }
  }
}
