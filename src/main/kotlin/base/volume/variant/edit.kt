package xyz.kemonomachi.mokuroku.base.volume.variant.edit

import android.os.Bundle
import androidx.navigation.compose.navArgument
import androidx.navigation.NavType

import xyz.kemonomachi.mokuroku.C

internal object Args {
  val id = navArgument(C.id) { type = NavType.LongType }
}

internal fun Bundle.getId() = getLong(Args.id.name)
