package xyz.kemonomachi.mokuroku.base.volume.create_edit

import java.time.LocalDate

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ListItem
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel as AndroidViewModel
import androidx.lifecycle.ViewModelProvider

import com.vanpra.composematerialdialogs.datetime.date.datepicker
import com.vanpra.composematerialdialogs.MaterialDialog

import xyz.kemonomachi.mokuroku.base.Displayable
import xyz.kemonomachi.mokuroku.base.Series
import xyz.kemonomachi.mokuroku.base.series.DAO as SeriesDAO
import xyz.kemonomachi.mokuroku.base.Volume
import xyz.kemonomachi.mokuroku.base.volume.DAO as VolumeDAO
import xyz.kemonomachi.mokuroku.creditable.Entity as CreditableEntity
import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.data.ISBN
import xyz.kemonomachi.mokuroku.R
import xyz.kemonomachi.mokuroku.ui.components.DoneButton
import xyz.kemonomachi.mokuroku.ui.components.DoneTextField
import xyz.kemonomachi.mokuroku.ui.components.DropDown
import xyz.kemonomachi.mokuroku.ui.components.NextTextField
import xyz.kemonomachi.mokuroku.ui.components.Scaffold
import xyz.kemonomachi.mokuroku.ui.components.UIList
import xyz.kemonomachi.mokuroku.work.Entity as WorkEntity

@Composable
internal fun <VOLUME: Volume, SERIES: Series<SERIES>> Screen(
  viewModel: ViewModel<VOLUME, SERIES>,
  titleId: Int,
  createSeries: (String, Long) -> SERIES,
  navigationIcon: @Composable () -> Unit
) =
  Scaffold(
    title = stringResource(titleId),
    navigationIcon = navigationIcon,
    actions = {
      DoneButton { viewModel.saveAndExit() }
    }
  ) {
    ISBNField(viewModel.isbn)

    WorkAndSeriesDropDowns(
      works = viewModel.allWorks,
      selectedWork = viewModel.work,
      seriesInWork = viewModel.seriesInWork,
      selectedSeries = viewModel.series,
      createSeries = createSeries
    )

    val volumeFocusRequester = FocusRequester()

    TitleField(viewModel.title, volumeFocusRequester)

    VolumeField(
      volume = viewModel.volume,
      focusRequester = volumeFocusRequester,
      onDone = { viewModel.saveAndExit() }
    )

    PublicationDatePicker(viewModel.publicationDate)

    AuthorSelector(
      allCreditables = viewModel.allCreditables,
      authors = viewModel.authors,
      addedAuthors = viewModel.addedAuthors,
      deletedAuthors = viewModel.deletedAuthors
    )
  }

internal class ViewModelFactory<VOLUME: Volume, SERIES: Series<SERIES>>(
  private val createViewModel: () -> ViewModel<VOLUME, SERIES>
) : ViewModelProvider.Factory {
  @Suppress("UNCHECKED_CAST")
  override fun <T : AndroidViewModel> create(modelClass: Class<T>) =
    createViewModel() as T
}

internal abstract class ViewModel<VOLUME: Volume, SERIES: Series<SERIES>>(
  protected val repo: Repo<VOLUME, SERIES>,
) : AndroidViewModel() {
  abstract val isbn: MutableLiveData<ISBN?>

  val title = MediatorLiveData<String?>()
  val volume = MediatorLiveData<Int?>()
  val publicationDate = MediatorLiveData<LocalDate?>()
  val work = MediatorLiveData<WorkEntity?>()
  val series = MediatorLiveData<SERIES?>()

  val allWorks = AppDatabase.workDAO().all()
  val allCreditables = AppDatabase.creditableDAO().all()
  val seriesInWork = Transformations.switchMap(work) {
    it?.let { repo.seriesDAO.byWork(it.id) }
  }

  val authors = MediatorLiveData<List<CreditableEntity>>().apply {
    setValue(emptyList())
  }
  val addedAuthors = MediatorLiveData<List<CreditableEntity>>().apply {
    setValue(emptyList())
  }
  val deletedAuthors = MutableLiveData<List<CreditableEntity>>(emptyList())

  init {
    series.addSource(Transformations.distinctUntilChanged(work)) {
      series.setValue(null)
    }

    title.addSource(series) {
      if(title.getValue().isNullOrEmpty()) title.setValue(series.value?.title)
    }
  }

  abstract fun saveAndExit()
}

internal interface Repo<VOLUME: Volume, SERIES: Series<SERIES>> {
  val volumeDAO: VolumeDAO<VOLUME, SERIES>
  val seriesDAO: SeriesDAO<SERIES>
}

@Composable
private fun ISBNField(isbn: MutableLiveData<ISBN?>) {
  val state by isbn.observeAsState(initial = ISBN(""))

  NextTextField(
    value = state?.toString() ?: "",
    onValueChange = { isbn.setValue(ISBN(it)) },
    labelId = R.string.isbn
  )
}

@Composable
private fun <E: Series<*>> WorkAndSeriesDropDowns(
  works: LiveData<List<WorkEntity>>,
  selectedWork: MutableLiveData<WorkEntity?>,
  seriesInWork: LiveData<List<E>>,
  selectedSeries: MutableLiveData<E?>,
  createSeries: (String, Long) -> E
) {
  val selectedWorkState: WorkEntity? by selectedWork.observeAsState()

  WorkDropDown(
    works = works,
    selectedWork = selectedWorkState,
    onSelection = selectedWork::setValue
  )

  SeriesDropDown(
    series = seriesInWork,
    selectedSeries = selectedSeries,
    work = selectedWorkState,
    createSeries = createSeries
  )
}

@Composable
private fun WorkDropDown(
  works: LiveData<List<WorkEntity>>,
  selectedWork: WorkEntity?,
  onSelection: (WorkEntity) -> Unit
) {
  val worksState by works.observeAsState(initial = emptyList())

  DropDown(
    elements = worksState,
    selectedValue = selectedWork,
    createElement = { WorkEntity(title = it) },
    onSelection = onSelection
  )
}

@Composable
private fun <E: Series<*>> SeriesDropDown(
  series: LiveData<List<E>>,
  selectedSeries: MutableLiveData<E?>,
  work: WorkEntity?,
  createSeries: (String, Long) -> E
) {
  val seriesState by series.observeAsState(initial = emptyList())
  val selectedSeriesState by selectedSeries.observeAsState()

  DropDown(
    elements = seriesState,
    selectedValue = selectedSeriesState,
    createElement = { createSeries(it, work!!.id) },
    onSelection = selectedSeries::setValue,
    enabled = work != null
  )
}

@Composable
private fun TitleField(
  title: MutableLiveData<String?>,
  focusRequester: FocusRequester
) {
  val state by title.observeAsState(initial = "")

  NextTextField(
    value = state ?: "",
    onValueChange = { title.setValue(it) },
    labelId = R.string.title,
    focusRequester = focusRequester
  )
}

@Composable
private fun VolumeField(
  volume: MutableLiveData<Int?>,
  focusRequester: FocusRequester,
  onDone: () -> Unit
) {
  val volumeState by volume.observeAsState()

  DoneTextField(
    value = volumeState?.toString() ?: "",
    onValueChange = { volume.setValue(it.toIntOrNull()) },
    labelId = R.string.volume,
    keyboardType = KeyboardType.Number,
    modifier = Modifier.focusRequester(focusRequester),
    onDone = onDone
  )
}

@Composable
private fun PublicationDatePicker(date: MutableLiveData<LocalDate?>) {
  val state by date.observeAsState()
  val dialog = remember { MaterialDialog() }

  dialog.build(
    buttons = {
      positiveButton(stringResource(R.string.select))
      negativeButton(stringResource(R.string.cancel))
    }
  ) {
    datepicker(initialDate = state ?: LocalDate.now()) { date.setValue(it) }
  }

  TextButton(
    onClick = { dialog.show() },
    modifier = Modifier.padding(vertical = 10.dp)
  ) { Text(state?.toString() ?: stringResource(R.string.publication_date)) }
}

@Composable
private fun AuthorSelector(
  allCreditables: LiveData<List<CreditableEntity>>,
  authors: LiveData<List<CreditableEntity>>,
  addedAuthors: MutableLiveData<List<CreditableEntity>>,
  deletedAuthors: MutableLiveData<List<CreditableEntity>>,
) {
  val allCreditablesState by allCreditables.observeAsState(initial = emptyList())
  val authorsState by authors.observeAsState(initial = emptyList())
  val addedAuthorsState by addedAuthors.observeAsState(initial = emptyList())
  val deletedAuthorsState by deletedAuthors.observeAsState(initial = emptyList())

  MultiSelector(
    titleId = R.string.author,
    elements = allCreditablesState,
    originalSelection = authorsState,
    added = addedAuthorsState,
    deleted = deletedAuthorsState,
    createElement = { CreditableEntity(name = it) },
    onAddition = { addedAuthors.setValue(it) },
    onDeletion = { deletedAuthors.setValue(it) }
  )
}

@Composable
@OptIn(ExperimentalMaterialApi::class)
internal fun <E: Displayable> MultiSelector(
  titleId: Int,
  elements: List<E>,
  originalSelection: List<E>,
  added: List<E>,
  deleted: List<E>,
  createElement: (String) -> E,
  onAddition: (List<E>) -> Unit,
  onDeletion: (List<E>) -> Unit
) {
  Column {
    Text(stringResource(titleId))

    UIList(originalSelection - deleted + added) {
      Card {
        ListItem(
          trailing = {
            Button({
              if(it in originalSelection) onDeletion(deleted + it)
              else onAddition(added - it)
            }) {
              Text(stringResource(R.string.delete))
            }
          }
        ) { Text(it.text) }
      }
    }

    DropDown(
      elements = elements - originalSelection - added + deleted,
      selectedValue = null,
      createElement = createElement,
      onSelection = {
        if(it in originalSelection) onDeletion(deleted - it)
        else onAddition(added + it)
      }
    )
  }
}
