package xyz.kemonomachi.mokuroku.base.volume.details

import android.os.Bundle
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.ListItem
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.navigation.compose.navArgument
import androidx.navigation.NavType

import xyz.kemonomachi.mokuroku.base.Series
import xyz.kemonomachi.mokuroku.base.Volume
import xyz.kemonomachi.mokuroku.base.volume.Variant
import xyz.kemonomachi.mokuroku.C
import xyz.kemonomachi.mokuroku.R
import xyz.kemonomachi.mokuroku.ui.components.CreditableCard
import xyz.kemonomachi.mokuroku.ui.components.DictItem
import xyz.kemonomachi.mokuroku.ui.components.EditButton
import xyz.kemonomachi.mokuroku.ui.components.LabeledList
import xyz.kemonomachi.mokuroku.ui.components.NavAction
import xyz.kemonomachi.mokuroku.ui.components.Scaffold

@Composable
@OptIn(ExperimentalMaterialApi::class)
internal fun Screen(
  state: Details?,
  toVolumeEdit: () -> Unit,
  toCreditableDetails: (Long) -> Unit,
  addVariant: () -> Unit,
  floatingActionButton: @Composable () -> Unit,
  navigationIcon: @Composable () -> Unit
) {
  state?.run {
    Scaffold(
      floatingActionButton = floatingActionButton,
      title = "${series.title} ${volume.volume}",
      navigationIcon = navigationIcon,
      actions = {
        AddButton(addVariant)
        EditButton(toVolumeEdit)
      }
    ) {
      DictItem(R.string.isbn, volume.isbn)
      DictItem(R.string.series, series.title)
      DictItem(R.string.title, volume.title)
      DictItem(R.string.volume, volume.volume)
      DictItem(R.string.publication_date, volume.publicationDate ?: "")

      LabeledList(R.string.variant_list, variants) {
        Card { ListItem { Text(it.name) } }
      }

      LabeledList(R.string.author, authors) { CreditableCard(it, toCreditableDetails) }
    }
  }
}

@Composable
private fun AddButton(toAddScreen: () -> Unit) =
  NavAction(Icons.Filled.Add, R.string.add_variant_action, toAddScreen)

internal interface Details {
  val volume: Volume
  val series: Series<*>
  val variants: List<Variant>
  val authors: List<xyz.kemonomachi.mokuroku.creditable.Entity>
}

internal object Args {
  val id = navArgument(C.id) { type = NavType.LongType }
}

internal fun Bundle.getId() = getLong(Args.id.name)
