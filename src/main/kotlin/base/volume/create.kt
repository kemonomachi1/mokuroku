package xyz.kemonomachi.mokuroku.base.volume.create

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import androidx.navigation.compose.navArgument
import androidx.navigation.NavType

import xyz.kemonomachi.mokuroku.base.Series
import xyz.kemonomachi.mokuroku.base.series.edit.Data as SeriesEditData
import xyz.kemonomachi.mokuroku.base.Volume
import xyz.kemonomachi.mokuroku.base.volume.create_edit.Repo
import xyz.kemonomachi.mokuroku.base.volume.create_edit.ViewModel as BaseViewModel
import xyz.kemonomachi.mokuroku.C
import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.data.ISBN
import xyz.kemonomachi.mokuroku.data.observeFirstValue

internal object Args {
  internal val isbn = navArgument(C.isbn) { type = NavType.StringType }
  internal val work = navArgument(C.work) { type = NavType.LongType }
  internal val series = navArgument(C.series) { type = NavType.LongType }
  internal val volume = navArgument(C.volume) { type = NavType.LongType }
}

internal fun Bundle.getISBN() = ISBN(getString(Args.isbn.name)!!)
internal fun Bundle.getWorkId() = getLong(Args.work.name)
internal fun Bundle.getSeriesId() = getLong(Args.series.name)
internal fun Bundle.getVolumeId() = getLong(Args.volume.name)

internal interface ViewModel {
  class FromScratch<VOLUME: Volume, SERIES: Series<SERIES>>(
    isbn: ISBN,
    repo: Repo<VOLUME, SERIES>,
    toVolume: (Long) -> Unit
  ) : Base<VOLUME, SERIES>(isbn, repo, toVolume)

  class InWork<VOLUME: Volume, SERIES: Series<SERIES>>(
    workId: Long,
    isbn: ISBN,
    repo: Repo<VOLUME, SERIES>,
    toVolume: (Long) -> Unit
  ) : Base<VOLUME, SERIES>(isbn, repo, toVolume) {
    init {
      initWork(workId)
    }
  }

  class InSeries<VOLUME: Volume, SERIES: Series<SERIES>>(
    seriesId: Long,
    isbn: ISBN,
    repo: Repo<VOLUME, SERIES>,
    toVolume: (Long) -> Unit
  ) : Base<VOLUME, SERIES>(isbn, repo, toVolume) {
    init {
      initSeries(repo.seriesDAO.edit(seriesId))
    }
  }

  class FromVolume<VOLUME: Volume, SERIES: Series<SERIES>>(
    volumeId: Long,
    isbn: ISBN,
    repo: Repo<VOLUME, SERIES>,
    toVolume: (Long) -> Unit
  ) : Base<VOLUME, SERIES>(isbn, repo, toVolume) {
    init {
      initSeries(repo.volumeDAO.editSeries(volumeId))
    }
  }

  open class Base<VOLUME: Volume, SERIES: Series<SERIES>>(
    isbn: ISBN,
    repo: Repo<VOLUME, SERIES>,
    private val toVolume: (Long) -> Unit
  ) : BaseViewModel<VOLUME, SERIES>(repo) {
    override val isbn = MutableLiveData<ISBN?>(isbn)

    protected fun initSeries(editData: LiveData<out SeriesEditData<SERIES>>) {
      work.observeFirstValue(editData) { it.work }
      series.observeFirstValue(editData) { it.series }

      Transformations.switchMap(series) {
        it?.let { repo.seriesDAO.authors(it.id) }
      }.let {
        addedAuthors.observeFirstValue(it) { it }
      }
    }

    protected fun initWork(workId: Long) =
      work.observeFirstValue(AppDatabase.workDAO().edit(workId)) { it }

    override fun saveAndExit() {
      viewModelScope.launch(Dispatchers.IO) {
        val id = repo.volumeDAO.insert(
          isbn = isbn.value!!,
          title = title.value!!,
          volume = volume.value!!,
          series = series.value!!,
          work = work.value!!,
          publicationDate = publicationDate.value!!,
          authors = addedAuthors.value!!
        )

        withContext(Dispatchers.Main) { toVolume(id) }
      }
    }
  }
}
