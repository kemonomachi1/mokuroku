package xyz.kemonomachi.mokuroku.base.volume

import java.time.LocalDate

import androidx.lifecycle.LiveData

import xyz.kemonomachi.mokuroku.base.Series
import xyz.kemonomachi.mokuroku.base.series.edit.Data as SeriesEditData
import xyz.kemonomachi.mokuroku.base.Volume
import xyz.kemonomachi.mokuroku.base.volume.edit.Data as EditData
import xyz.kemonomachi.mokuroku.creditable.Entity as CreditableEntity
import xyz.kemonomachi.mokuroku.data.ISBN
import xyz.kemonomachi.mokuroku.data.DAO as BaseDAO
import xyz.kemonomachi.mokuroku.work.Entity as WorkEntity

internal abstract class DAO<VOLUME: Volume, SERIES: Series<SERIES>> : BaseDAO<VOLUME>() {
  abstract fun edit(id: Long): LiveData<out EditData<SERIES>>
  abstract fun editSeries(id: Long): LiveData<out SeriesEditData<SERIES>>

  abstract suspend fun insert(
    isbn: ISBN,
    title: String,
    volume: Int,
    publicationDate: LocalDate,
    series: SERIES,
    work: WorkEntity,
    authors: List<CreditableEntity>
  ): Long

  abstract suspend fun update(
    id: Long,
    isbn: ISBN,
    title: String,
    volume: Int,
    publicationDate: LocalDate,
    series: SERIES,
    work: WorkEntity,
    addedAuthors: List<CreditableEntity>,
    deletedAuthors: List<CreditableEntity>
  )
}

internal interface Variant {
  val name: String
}
