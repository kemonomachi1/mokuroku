package xyz.kemonomachi.mokuroku.base.series.edit

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

import android.os.Bundle
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel as AndroidViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.navigation.compose.navArgument
import androidx.navigation.NavType

import xyz.kemonomachi.mokuroku.base.Series
import xyz.kemonomachi.mokuroku.base.series.DAO as SeriesDAO
import xyz.kemonomachi.mokuroku.C
import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.data.observeFirstValue
import xyz.kemonomachi.mokuroku.R
import xyz.kemonomachi.mokuroku.ui.components.DoneButton
import xyz.kemonomachi.mokuroku.ui.components.DoneTextField
import xyz.kemonomachi.mokuroku.ui.components.DropDown
import xyz.kemonomachi.mokuroku.ui.components.Scaffold
import xyz.kemonomachi.mokuroku.work.Entity as WorkEntity

internal interface Data<SERIES: Series<SERIES>> {
  val series: SERIES
  val work: WorkEntity
}

internal object Args {
  val id = navArgument(C.id) { type = NavType.LongType }
}

internal fun Bundle.getId() = getLong(Args.id.name)

@Composable
internal fun <SERIES: Series<SERIES>> Screen(
  titleId: Int,
  navigationIcon: @Composable () -> Unit,
  createViewModel: () -> ViewModel<SERIES>
) {
  val viewModel: ViewModel<SERIES> =
    viewModel(factory = ViewModelFactory(createViewModel))

  Scaffold(
    title = stringResource(titleId),
    navigationIcon = navigationIcon,
    actions = {
      DoneButton { viewModel.saveAndExit() }
    }
  ) {
    val work: WorkEntity? by viewModel.work.observeAsState()
    val allWorks by viewModel.allWorks.observeAsState(initial = emptyList())
    val title by viewModel.title.observeAsState(initial = "")

    DropDown(
      elements = allWorks,
      selectedValue = work,
      createElement = { WorkEntity(title = it) },
      onSelection = { viewModel.work.setValue(it) }
    )

    DoneTextField(
      value = title,
      onValueChange = { viewModel.title.setValue(it) },
      labelId = R.string.title,
      onDone = {
        viewModel.saveAndExit()
      }
    )
  }
}

internal class ViewModelFactory<SERIES: Series<SERIES>>(
  private val createViewModel: () -> ViewModel<SERIES>
) : ViewModelProvider.Factory {
  @Suppress("UNCHECKED_CAST")
  override fun <T : AndroidViewModel> create(modelClass: Class<T>) =
    createViewModel() as T
}

internal class ViewModel<SERIES: Series<SERIES>>(
  private val id: Long,
  val seriesDAO: SeriesDAO<SERIES>,
  private val exit: () -> Unit
) : AndroidViewModel() {
  val allWorks = AppDatabase.workDAO().all()

  val title = MediatorLiveData<String>()
  var work = MediatorLiveData<WorkEntity?>()

  init {
    seriesDAO.edit(id).let {
      title.observeFirstValue(it) { it.series.title }
      work.observeFirstValue(it) { it.work }
    }
  }

  fun saveAndExit() =
    viewModelScope.launch(Dispatchers.IO) {
      seriesDAO.update(id, title.value!!, work.value!!)

      withContext(Dispatchers.Main) { exit() }
    }
}
