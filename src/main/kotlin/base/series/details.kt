package xyz.kemonomachi.mokuroku.base.series.details

import android.os.Bundle
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ListItem
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.navigation.compose.navArgument
import androidx.navigation.NavType

import xyz.kemonomachi.mokuroku.base.Series
import xyz.kemonomachi.mokuroku.base.Volume
import xyz.kemonomachi.mokuroku.C
import xyz.kemonomachi.mokuroku.R
import xyz.kemonomachi.mokuroku.ui.components.DictItem
import xyz.kemonomachi.mokuroku.ui.components.EditButton
import xyz.kemonomachi.mokuroku.ui.components.LabeledList
import xyz.kemonomachi.mokuroku.ui.components.NavCard
import xyz.kemonomachi.mokuroku.ui.components.Scaffold
import xyz.kemonomachi.mokuroku.work.Entity as WorkEntity

@Composable
@OptIn(ExperimentalMaterialApi::class)
internal fun Screen(
  state: Details?,
  toVolumeDetails: (Long) -> Unit,
  toSeriesEdit: () -> Unit,
  floatingActionButton: @Composable () -> Unit,
  navigationIcon: @Composable () -> Unit
) {
  state?.run {
    Scaffold(
      floatingActionButton = floatingActionButton,
      title = series.title,
      navigationIcon = navigationIcon,
      actions = { EditButton(toSeriesEdit) }
    ) {
      DictItem(R.string.work, work.title)
      DictItem(R.string.title, series.title)

      LabeledList(
        titleId = R.string.volume_list,
        items = volumes
          .sortedWith(compareBy({ it.volume }, { it.title }))
      ) {
        NavCard({ toVolumeDetails(it.id) }) { ListItem(
          text = { Text(it.title) },
          trailing = { Text(it.volume.toString()) }
        ) }
      }
    }
  }
}

internal interface Details {
  val series: Series<*>
  val work: WorkEntity
  val volumes: List<Volume>
}

internal object Args {
  val id = navArgument(C.id) { type = NavType.LongType }
}

internal fun Bundle.getId() = getLong(Args.id.name)
