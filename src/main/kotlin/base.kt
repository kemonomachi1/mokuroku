package xyz.kemonomachi.mokuroku.base

import java.time.LocalDate

import android.os.Bundle
import androidx.compose.runtime.Composable
import androidx.navigation.compose.composable as androidComposable
import androidx.navigation.compose.NamedNavArgument
import androidx.navigation.NavGraphBuilder
import androidx.room.Ignore

import xyz.kemonomachi.mokuroku.data.ISBN

internal interface Volume : Identifiable {
  val isbn: ISBN
  val title: String
  val volume: Int
  val publicationDate: LocalDate?
}

internal abstract class Series<T : Series<T>> : Identifiable, Displayable {
  abstract val title: String
  @delegate:Ignore override val text by this::title

  abstract fun withWork(workId: Long): T
}

internal fun NavGraphBuilder.composable(
  route: String,
  vararg arguments: NamedNavArgument,
  content: @Composable (Bundle) -> Unit
) =
  androidComposable(route, arguments.toList()) {
    it.arguments?.let { content(it) }
  }

internal interface Identifiable {
  val id: Long
}

internal interface Displayable {
  val text: String
}
