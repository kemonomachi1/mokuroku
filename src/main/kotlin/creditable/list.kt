package xyz.kemonomachi.mokuroku.creditable.list

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.ViewModel as AndroidViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.composable
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder

import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.R
import xyz.kemonomachi.mokuroku.creditable.details.route as creditableDetailsRoute
import xyz.kemonomachi.mokuroku.ui.components.CreditableCard
import xyz.kemonomachi.mokuroku.ui.components.UIList
import xyz.kemonomachi.mokuroku.ui.components.Scaffold
import xyz.kemonomachi.mokuroku.ui.components.ScanFAB
import xyz.kemonomachi.mokuroku.ui.components.UpButton

internal const val ROUTE = "creditable/list"

internal fun NavGraphBuilder.destination(navCon: NavController) =
  composable(ROUTE) { Screen(
    toCreditableDetails = { navCon.navigate(creditableDetailsRoute(it)) },
    floatingActionButton = { ScanFAB(navCon) },
    navigationIcon = { UpButton(navCon) }
  ) }

@Composable
private fun Screen(
  toCreditableDetails: (Long) -> Unit,
  floatingActionButton: @Composable () -> Unit,
  navigationIcon: @Composable () -> Unit
) {
  Scaffold(
    floatingActionButton = floatingActionButton,
    title = stringResource(R.string.creditable),
    navigationIcon = navigationIcon
  ) {
    val viewModel: ViewModel = viewModel()
    val creditables by viewModel.creditables.observeAsState(initial = emptyList())

    UIList(creditables) { CreditableCard(it, toCreditableDetails) }
  }
}

internal class ViewModel : AndroidViewModel() {
  val creditables = AppDatabase.creditableDAO().all()
}
