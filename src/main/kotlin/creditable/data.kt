package xyz.kemonomachi.mokuroku.creditable

import kotlin.collections.List

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Entity as RoomEntity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import androidx.room.Query

import xyz.kemonomachi.mokuroku.base.Displayable
import xyz.kemonomachi.mokuroku.base.Identifiable
import xyz.kemonomachi.mokuroku.data.DAO as BaseDAO

@RoomEntity(tableName = "creditables")
internal data class Entity(
  @PrimaryKey(autoGenerate = true) override val id: Long = 0,
  val name: String
) : Identifiable, Displayable {
  @delegate:Ignore override val text by this::name
}

@Dao
internal abstract class DAO : BaseDAO<Entity>() {
  @Query("select id,name from creditables")
  abstract fun all(): LiveData<List<Entity>>

  @Query("select count(*) from creditables")
  abstract fun count(): LiveData<Int>

  @Query("select id,name from creditables where id = :id")
  abstract fun details(id: Long): LiveData<Entity>

  @Query("select id,name from creditables where id = :id")
  abstract suspend fun edit(id: Long): Entity

  suspend fun update(id: Long, name: String) = update(Entity(id, name))
}
