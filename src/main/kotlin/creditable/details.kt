package xyz.kemonomachi.mokuroku.creditable.details

import android.os.Bundle
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.lifecycle.ViewModel as AndroidViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.navArgument
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType

import xyz.kemonomachi.mokuroku.base.composable
import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.C
import xyz.kemonomachi.mokuroku.creditable.edit.route as creditableEditRoute
import xyz.kemonomachi.mokuroku.R
import xyz.kemonomachi.mokuroku.ui.components.DictItem
import xyz.kemonomachi.mokuroku.ui.components.EditButton
import xyz.kemonomachi.mokuroku.ui.components.Scaffold
import xyz.kemonomachi.mokuroku.ui.components.ScanFAB
import xyz.kemonomachi.mokuroku.ui.components.UpButton

private object Args {
  val id = navArgument(C.id) { type = NavType.LongType }
}

private fun Bundle.getId() = getLong(Args.id.name)

private const val BASE_ROUTE = "creditable/details"
private val ROUTE = "$BASE_ROUTE/{${Args.id.name}}"

internal fun route(id: Long) = "$BASE_ROUTE/$id"

internal fun NavGraphBuilder.destination(navCon: NavController) =
  composable(ROUTE, Args.id) {
    val id = it.getId()

    Screen(
      id = id,
      toCreditableEdit = { navCon.navigate(creditableEditRoute(id)) },
      floatingActionButton = { ScanFAB(navCon) },
      navigationIcon = { UpButton(navCon) }
    )
  }

@Composable
private fun Screen(
  id: Long,
  toCreditableEdit: () -> Unit,
  floatingActionButton: @Composable () -> Unit,
  navigationIcon: @Composable () -> Unit
) {
  val viewModel: ViewModel = viewModel(factory = ViewModelFactory(id))
  val state by viewModel.creditable.observeAsState()

  state?.run {
    Scaffold(
      floatingActionButton = floatingActionButton,
      title = name,
      navigationIcon = navigationIcon,
      actions = { EditButton(toCreditableEdit) }
    ) {
      DictItem(R.string.name, name)
    }
  }
}

private class ViewModelFactory(
  private val id: Long
) : ViewModelProvider.Factory {

  @Suppress("UNCHECKED_CAST")
  override fun <T : AndroidViewModel> create(modelClass: Class<T>) =
    ViewModel(id) as T
}

internal class ViewModel(val id: Long) : AndroidViewModel() {
  val creditable = AppDatabase.creditableDAO().details(id)
}
