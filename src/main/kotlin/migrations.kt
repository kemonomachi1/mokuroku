package xyz.kemonomachi.mokuroku

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

internal val MIGRATION_1_2 = object : Migration(1, 2) {
  override fun migrate(database: SupportSQLiteDatabase) {
    database.execSQL("""
      create table manga_series (
        id integer primary key autoincrement not null,
        title text not null
      )
    """.trimIndent())

    database.execSQL("""
      create table manga_volumes (
        id integer primary key autoincrement not null,
        isbn text not null,
        title text not null,
        volume integer not null,
        manga_series_id integer,
        foreign key (manga_series_id)
          references manga_series (id)
          on update no action
          on delete no action
      )
    """.trimIndent())

    database.execSQL("""
      create index index_manga_volumes_manga_series_id
      on manga_volumes (manga_series_id)
    """.trimIndent())

    database.execSQL("""
      insert into manga_volumes (id, isbn, title, volume)
      select id,isbn,title,volume from manga
    """.trimIndent())

    database.execSQL("drop table manga")
  }
}

internal val MIGRATION_2_3 = object : Migration(2, 3) {
  override fun migrate(database: SupportSQLiteDatabase) {
    database.execSQL("""
      create table manga_volume_variants (
        id integer primary key autoincrement not null,
        name text not null,
        manga_volume_id integer not null,
        foreign key (manga_volume_id)
          references manga_volumes (id)
          on update no action
          on delete no action
      )
    """.trimIndent())

    database.execSQL("""
      create index index_manga_volume_variants_manga_volume_id
      on manga_volume_variants (manga_volume_id)
    """.trimIndent())
  }
}

internal val MIGRATION_3_4 = object : Migration(3, 4) {
  override fun migrate(database: SupportSQLiteDatabase) {
    database.execSQL("""
      create table new_manga_volumes (
        id integer primary key autoincrement not null,
        isbn text not null,
        title text not null,
        volume integer not null,
        manga_series_id integer not null,
        foreign key (manga_series_id)
          references manga_series (id)
          on update no action
          on delete no action
      )
    """.trimIndent())

    database.execSQL("""
      insert into new_manga_volumes (id, isbn, title, volume, manga_series_id)
      select id,isbn,title,volume,manga_series_id from manga_volumes
    """.trimIndent())

    database.execSQL("drop table manga_volumes")

    database.execSQL("alter table new_manga_volumes rename to manga_volumes")

    database.execSQL("""
      create index index_manga_volumes_manga_series_id
      on manga_volumes (manga_series_id)
    """.trimIndent())
  }
}

internal val MIGRATION_4_5 = object : Migration(4, 5) {
  override fun migrate(database: SupportSQLiteDatabase) {
    database.execSQL("""
      create table works (
        id integer primary key autoincrement not null,
        title text not null
      )
    """.trimIndent())

    database.execSQL("""
      alter table manga_series add column
        work_id integer
        references works (id)
        on update no action
        on delete no action
    """.trimIndent())

    database.execSQL("""
      create index index_manga_series_work_id
      on manga_series (work_id)
    """.trimIndent())
  }
}

internal val MIGRATION_5_6 = object : Migration(5, 6) {
  override fun migrate(database: SupportSQLiteDatabase) {
    // create view is extra fussy, backticks and capitalized 'AS' are required
    database.execSQL("""
      create view `MangaSeriesWithVolumeCount` AS select
        manga_series.id,
        manga_series.title,
        manga_series.work_id,
        count(manga_volumes.id) as volume_count
      from manga_series
      left join manga_volumes
      on manga_series.id = manga_volumes.manga_series_id
      group by manga_series.id
    """.trimIndent())
  }
}

internal val MIGRATION_6_7 = object : Migration(6, 7) {
  override fun migrate(database: SupportSQLiteDatabase) {
    database.execSQL("""
      create table new_manga_series (
        id integer primary key autoincrement not null,
        title text not null,
        work_id integer not null,
        foreign key (work_id)
          references works (id)
          on update no action
          on delete no action
      )
    """.trimIndent())

    database.execSQL("""
      insert into new_manga_series (id, title, work_id)
      select id,title,work_id from manga_series
    """.trimIndent())

    database.execSQL("drop table manga_series")

    database.execSQL("alter table new_manga_series rename to manga_series")

    database.execSQL("""
      create index index_manga_series_work_id
      on manga_series (work_id)
    """.trimIndent())
  }
}

internal val MIGRATION_7_8 = object : Migration(7, 8) {
  override fun migrate(database: SupportSQLiteDatabase) {
    database.execSQL("""
      create table creditables (
        id integer primary key autoincrement not null,
        name text not null
      )
    """.trimIndent())

    database.execSQL("""
      create table manga_volume_authors (
        manga_volume_id integer not null,
        author_id integer not null,
        primary key (manga_volume_id, author_id),
        foreign key (manga_volume_id)
          references manga_volumes (id)
          on update no action
          on delete no action
        foreign key (author_id)
          references creditables (id)
          on update no action
          on delete no action
      )
    """.trimIndent())

    database.execSQL("""
      create index index_manga_volume_authors_manga_volume_id
      on manga_volume_authors (manga_volume_id)
    """.trimIndent())

    database.execSQL("""
      create index index_manga_volume_authors_author_id
      on manga_volume_authors (author_id)
    """.trimIndent())
  }
}

internal val MIGRATION_8_9 = object : Migration(8, 9) {
  override fun migrate(database: SupportSQLiteDatabase) {
    database.execSQL("""
      create table novel_series (
        id integer primary key autoincrement not null,
        title text not null,
        work_id integer not null,
        foreign key (work_id)
          references works (id)
          on update no action
          on delete no action
      )
    """.trimIndent())

    database.execSQL("""
      create index index_novel_series_work_id
      on novel_series (work_id)
    """.trimIndent())

    // create view is extra fussy, backticks and capitalized 'AS' are required
    database.execSQL("""
      create view `NovelSeriesWithVolumeCount` AS select
        novel_series.id,
        novel_series.title,
        novel_series.work_id,
        count(novel_volumes.id) as volume_count
      from novel_series
      left join novel_volumes
      on novel_series.id = novel_volumes.novel_series_id
      group by novel_series.id
    """.trimIndent())

    database.execSQL("""
      create table novel_volumes (
        id integer primary key autoincrement not null,
        isbn text not null,
        title text not null,
        volume integer not null,
        novel_series_id integer not null,
        foreign key (novel_series_id)
          references novel_series (id)
          on update no action
          on delete no action
      )
    """.trimIndent())

    database.execSQL("""
      create index index_novel_volumes_novel_series_id
      on novel_volumes (novel_series_id)
    """.trimIndent())
  }
}

internal val MIGRATION_9_10 = object : Migration(9, 10) {
  override fun migrate(database: SupportSQLiteDatabase) {
    database.execSQL("""
      create table novel_volume_authors (
        novel_volume_id integer not null,
        author_id integer not null,
        primary key (novel_volume_id, author_id),
        foreign key (novel_volume_id)
          references novel_volumes (id)
          on update no action
          on delete no action
        foreign key (author_id)
          references creditables (id)
          on update no action
          on delete no action
      )
    """.trimIndent())

    database.execSQL("""
      create index index_novel_volume_authors_novel_volume_id
      on novel_volume_authors (novel_volume_id)
    """.trimIndent())

    database.execSQL("""
      create index index_novel_volume_authors_author_id
      on novel_volume_authors (author_id)
    """.trimIndent())
  }
}

internal val MIGRATION_10_11 = object : Migration(10, 11) {
  override fun migrate(database: SupportSQLiteDatabase) {
    database.execSQL("""
      create table novel_volume_variants (
        id integer primary key autoincrement not null,
        name text not null,
        novel_volume_id integer not null,
        foreign key (novel_volume_id)
          references novel_volumes (id)
          on update no action
          on delete no action
      )
    """.trimIndent())

    database.execSQL("""
      create index index_novel_volume_variants_novel_volume_id
      on novel_volume_variants (novel_volume_id)
    """.trimIndent())
  }
}

internal val MIGRATION_11_12 = object : Migration(11, 12) {
  override fun migrate(database: SupportSQLiteDatabase) {
    database.execSQL("""
      alter table manga_volumes add column
        publication_date text
    """.trimIndent())

    database.execSQL("""
      alter table novel_volumes add column
        publication_date text
    """.trimIndent())
  }
}
