package xyz.kemonomachi.mokuroku.contract

import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract

import com.google.zxing.client.android.Intents
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.CaptureActivity

import xyz.kemonomachi.mokuroku.data.ISBN

internal class ScanBarcode: ActivityResultContract<Unit, ISBN?> () {
  override fun createIntent(context: Context, _unit: Unit): Intent =
    Intent(context, CaptureActivity::class.java)
      .setAction(Intents.Scan.ACTION)
      .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_DOCUMENT)
      .putExtra(Intents.Scan.ORIENTATION_LOCKED, false)

  override fun parseResult(resultCode: Int, intent: Intent?) =
    IntentIntegrator
      .parseActivityResult(resultCode, intent)
      .getContents()
      ?.let { ISBN(it) }
}
