package xyz.kemonomachi.mokuroku.novel.series.details

import kotlin.collections.List

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.lifecycle.ViewModel as AndroidViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.room.Embedded
import androidx.room.Relation

import xyz.kemonomachi.mokuroku.base.composable
import xyz.kemonomachi.mokuroku.base.series.details.*
import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.novel.series.edit.route as editRoute
import xyz.kemonomachi.mokuroku.novel.volume.create.inSeriesRoute as volumeCreateRoute
import xyz.kemonomachi.mokuroku.novel.volume.details.route as volumeDetailsRoute
import xyz.kemonomachi.mokuroku.ui.components.ScanFAB
import xyz.kemonomachi.mokuroku.ui.components.UpButton

private const val BASE_ROUTE = "novel/series/details"
private val ROUTE = "$BASE_ROUTE/{${Args.id.name}}"

internal fun route(id: Long) = "$BASE_ROUTE/$id"

internal fun NavGraphBuilder.destination(navCon: NavController) =
  composable(ROUTE, Args.id) {
    val id = it.getId()

    Screen(
      id = id,
      toNovelVolumeDetails = { navCon.navigate(volumeDetailsRoute(it)) },
      toNovelSeriesEdit = { navCon.navigate(editRoute(id)) },
      floatingActionButton = { ScanFAB(navCon) { volumeCreateRoute(it, id) } },
      navigationIcon = { UpButton(navCon) }
    )
  }

internal data class Data(
  @Embedded
  override val series: xyz.kemonomachi.mokuroku.novel.series.Entity,

  @Relation(parentColumn = "work_id", entityColumn = "id")
  override val work: xyz.kemonomachi.mokuroku.work.Entity,

  @Relation(parentColumn = "id", entityColumn = "novel_series_id")
  override val volumes: List<xyz.kemonomachi.mokuroku.novel.volume.Entity>
) : Details

@Composable
private fun Screen(
  id: Long,
  toNovelVolumeDetails: (Long) -> Unit,
  toNovelSeriesEdit: () -> Unit,
  floatingActionButton: @Composable () -> Unit,
  navigationIcon: @Composable () -> Unit
) {
  val viewModel: ViewModel = viewModel(factory = ViewModelFactory(id))
  val state by viewModel.contents.observeAsState()

  Screen(
    state = state,
    toVolumeDetails = toNovelVolumeDetails,
    toSeriesEdit = toNovelSeriesEdit,
    floatingActionButton = floatingActionButton,
    navigationIcon = navigationIcon
  )
}

private class ViewModelFactory(
  private val id: Long
) : ViewModelProvider.Factory {

  @Suppress("UNCHECKED_CAST")
  override fun <T : AndroidViewModel> create(modelClass: Class<T>) =
    ViewModel(id) as T
}

internal class ViewModel(val id: Long) : AndroidViewModel() {
  val contents = AppDatabase.novelSeriesDAO().details(id)
}
