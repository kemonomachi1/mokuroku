package xyz.kemonomachi.mokuroku.novel.series.list

import androidx.room.ColumnInfo
import androidx.room.DatabaseView
import androidx.room.Embedded

import xyz.kemonomachi.mokuroku.base.series.ListElement

@DatabaseView(
  viewName = "NovelSeriesWithVolumeCount",
  value = """
select
  novel_series.id,
  novel_series.title,
  novel_series.work_id,
  count(novel_volumes.id) as volume_count
from novel_series
left join novel_volumes
on novel_series.id = novel_volumes.novel_series_id
group by novel_series.id
""")
internal data class Element(
  @Embedded override val series: xyz.kemonomachi.mokuroku.novel.series.Entity,
  @ColumnInfo(name = "volume_count") override val volumeCount: Int
) : ListElement
