package xyz.kemonomachi.mokuroku.novel.volume

import java.time.LocalDate

import androidx.lifecycle.LiveData
import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Entity as RoomEntity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Transaction

import xyz.kemonomachi.mokuroku.base.Volume
import xyz.kemonomachi.mokuroku.base.volume.DAO as VolumeDAO
import xyz.kemonomachi.mokuroku.creditable.Entity as CreditableEntity
import xyz.kemonomachi.mokuroku.data.ISBN
import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.novel.series.Entity as NovelSeriesEntity
import xyz.kemonomachi.mokuroku.novel.series.edit.Data as NovelSeriesEditData
import xyz.kemonomachi.mokuroku.novel.volume.details.Data as DetailsData
import xyz.kemonomachi.mokuroku.novel.volume.edit.Data as EditData
import xyz.kemonomachi.mokuroku.work.Entity as WorkEntity

@RoomEntity(
  tableName = "novel_volumes",
  indices = [Index(value = ["novel_series_id"])],
  foreignKeys = [
    ForeignKey(
      entity = xyz.kemonomachi.mokuroku.novel.series.Entity::class,
      parentColumns = ["id"],
      childColumns = ["novel_series_id"]
    )
  ]
)
internal data class Entity(
  @PrimaryKey(autoGenerate = true) override val id: Long = 0,
  override val isbn: ISBN,
  override val title: String,
  override val volume: Int,
  @ColumnInfo(name = "novel_series_id") val novelSeriesId: Long,
  @ColumnInfo(name = "publication_date") override val publicationDate: LocalDate?
) : Volume

@Dao
internal abstract class DAO : VolumeDAO<Entity, NovelSeriesEntity>() {
  @Transaction
  @Query("""
    select id,isbn,title,volume,novel_series_id,publication_date
    from novel_volumes
    where id = :id
  """)
  abstract override fun edit(id: Long): LiveData<EditData>

  @Transaction
  @Query("""
    select novel_series.id,novel_series.title,novel_series.work_id
    from novel_series
    inner join novel_volumes
    on novel_series.id = novel_volumes.novel_series_id
    where novel_volumes.id = :id
  """)
  abstract override fun editSeries(id: Long): LiveData<NovelSeriesEditData>

  @Transaction
  @Query("""
    select id,isbn,title,volume,novel_series_id,publication_date
    from novel_volumes
    where id = :id
  """)
  abstract fun details(id: Long): LiveData<DetailsData>

  @Transaction
  @Query("""
    select id,isbn,title,volume,novel_series_id,publication_date
    from novel_volumes
    where isbn = :isbn
  """)
  abstract suspend fun byISBN(isbn: ISBN): DetailsData?

  @Transaction
  override suspend fun insert(
    isbn: ISBN,
    title: String,
    volume: Int,
    publicationDate: LocalDate,
    series: NovelSeriesEntity,
    work: WorkEntity,
    authors: List<CreditableEntity>
  ) = insert(Entity(
    isbn = isbn,
    title = title,
    volume = volume,
    novelSeriesId = AppDatabase.novelSeriesDAO().ensure(
      series.withWork(AppDatabase.workDAO().ensure(work))
    ),
    publicationDate = publicationDate
  )).also {
    AppDatabase.novelVolumeAuthorDAO().insert(it, authors)
  }

  @Transaction
  override suspend fun update(
    id: Long,
    isbn: ISBN,
    title: String,
    volume: Int,
    publicationDate: LocalDate,
    series: NovelSeriesEntity,
    work: WorkEntity,
    addedAuthors: List<CreditableEntity>,
    deletedAuthors: List<CreditableEntity>
  ) {
    update(Entity(
      id = id,
      isbn = isbn,
      title = title,
      volume = volume,
      novelSeriesId = AppDatabase.novelSeriesDAO().ensure(
        series.withWork(AppDatabase.workDAO().ensure(work))
      ),
      publicationDate = publicationDate))

    with(AppDatabase.novelVolumeAuthorDAO()) {
      delete(id, deletedAuthors)
      insert(id, addedAuthors)
    }
  }
}
