package xyz.kemonomachi.mokuroku.novel.volume.create

import androidx.compose.runtime.Composable
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder

import xyz.kemonomachi.mokuroku.base.composable
import xyz.kemonomachi.mokuroku.base.volume.create.*
import xyz.kemonomachi.mokuroku.base.volume.create_edit.Screen as BaseScreen
import xyz.kemonomachi.mokuroku.base.volume.create_edit.ViewModel as BaseViewModel
import xyz.kemonomachi.mokuroku.base.volume.create_edit.ViewModelFactory
import xyz.kemonomachi.mokuroku.data.ISBN
import xyz.kemonomachi.mokuroku.entity_selection.FROM_SCRATCH_ROUTE as ENTITY_SELECTION_FROM_SCRATCH_ROUTE
import xyz.kemonomachi.mokuroku.entity_selection.IN_WORK_ROUTE as ENTITY_SELECTION_IN_WORK_ROUTE
import xyz.kemonomachi.mokuroku.novel.series.Entity as NovelSeriesEntity
import xyz.kemonomachi.mokuroku.novel.volume.create_edit.Repo
import xyz.kemonomachi.mokuroku.novel.volume.details.route as detailsRoute
import xyz.kemonomachi.mokuroku.novel.volume.Entity as NovelVolumeEntity
import xyz.kemonomachi.mokuroku.R
import xyz.kemonomachi.mokuroku.ui.components.UpButton

private const val BASE_ROUTE = "novel/volume/create"
private val FROM_SCRATCH_ROUTE = "$BASE_ROUTE/{${Args.isbn.name}}"
private val IN_WORK_ROUTE = "$FROM_SCRATCH_ROUTE/${Args.work.name}/{${Args.work.name}}"
private val IN_SERIES_ROUTE = "$FROM_SCRATCH_ROUTE/${Args.series.name}/{${Args.series.name}}"
private val FROM_VOLUME_ROUTE = "$FROM_SCRATCH_ROUTE/${Args.volume.name}/{${Args.volume.name}}"

internal fun fromScratchRoute(isbn: ISBN) = "$BASE_ROUTE/$isbn"
internal fun inWorkRoute(isbn: ISBN, workId: Long) =
  "${fromScratchRoute(isbn)}/${Args.work.name}/$workId"
internal fun inSeriesRoute(isbn: ISBN, seriesId: Long) =
  "${fromScratchRoute(isbn)}/${Args.series.name}/$seriesId"
internal fun fromVolumeRoute(isbn: ISBN, volumeId: Long) =
  "${fromScratchRoute(isbn)}/${Args.volume.name}/$volumeId"

private fun NavGraphBuilder.fromScratch(navCon: NavController) =
  composable(FROM_SCRATCH_ROUTE, Args.isbn) {
    Screen(navigationIcon = { UpButton(navCon) }) {
      ViewModel.FromScratch(it.getISBN(), Repo()) {
        toVolume(navCon, it, ENTITY_SELECTION_FROM_SCRATCH_ROUTE)
      }
    }
  }

private fun NavGraphBuilder.inWork(navCon: NavController) =
  composable(IN_WORK_ROUTE, Args.isbn, Args.work) {
    Screen(navigationIcon = { UpButton(navCon) }) {
      ViewModel.InWork(it.getWorkId(), it.getISBN(), Repo()) {
        toVolume(navCon, it, ENTITY_SELECTION_IN_WORK_ROUTE)
      }
    }
  }

private fun NavGraphBuilder.inSeries(navCon: NavController) =
  composable(IN_SERIES_ROUTE, Args.isbn, Args.series) {
    Screen(navigationIcon = { UpButton(navCon) }) {
      ViewModel.InSeries(it.getSeriesId(), it.getISBN(), Repo())  {
        toVolume(navCon, it, IN_SERIES_ROUTE)
      }
    }
  }

private fun NavGraphBuilder.fromVolume(navCon: NavController) =
  composable(FROM_VOLUME_ROUTE, Args.isbn, Args.volume) {
    Screen(navigationIcon = { UpButton(navCon) }) {
      ViewModel.FromVolume(it.getVolumeId(), it.getISBN(), Repo())  {
        toVolume(navCon, it, FROM_VOLUME_ROUTE)
      }
    }
  }

private fun toVolume(navCon: NavController, id: Long, originRoute: String) =
  navCon.navigate(detailsRoute(id)) {
    popUpTo(originRoute) { inclusive = true }
  }

internal fun NavGraphBuilder.destination(navCon: NavController) {
  fromScratch(navCon)
  inWork(navCon)
  inSeries(navCon)
  fromVolume(navCon)
}

@Composable
private fun Screen(
  navigationIcon: @Composable () -> Unit,
  createViewModel: () -> BaseViewModel<NovelVolumeEntity, NovelSeriesEntity>
) {
  BaseScreen<NovelVolumeEntity, NovelSeriesEntity>(
    viewModel = viewModel(factory = ViewModelFactory(createViewModel)),
    titleId = R.string.novel_volume,
    createSeries = {
      title, workId -> NovelSeriesEntity(title = title, workId = workId)
    },
    navigationIcon = navigationIcon
  )
}
