package xyz.kemonomachi.mokuroku.novel.volume.create_edit

import xyz.kemonomachi.mokuroku.base.volume.create_edit.Repo as BaseRepo
import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.novel.series.Entity as NovelSeriesEntity
import xyz.kemonomachi.mokuroku.novel.volume.Entity as NovelVolumeEntity

internal class Repo : BaseRepo<NovelVolumeEntity, NovelSeriesEntity> {
  override val volumeDAO = AppDatabase.novelVolumeDAO()
  override val seriesDAO = AppDatabase.novelSeriesDAO()
}
