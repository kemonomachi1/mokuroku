package xyz.kemonomachi.mokuroku.novel.volume.edit

import androidx.compose.runtime.Composable
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

import xyz.kemonomachi.mokuroku.base.composable
import xyz.kemonomachi.mokuroku.base.volume.create_edit.Screen as BaseScreen
import xyz.kemonomachi.mokuroku.base.volume.create_edit.ViewModel as BaseViewModel
import xyz.kemonomachi.mokuroku.base.volume.create_edit.ViewModelFactory
import xyz.kemonomachi.mokuroku.base.volume.edit.*
import xyz.kemonomachi.mokuroku.base.volume.edit.Data as BaseData
import xyz.kemonomachi.mokuroku.creditable.Entity as CreditableEntity
import xyz.kemonomachi.mokuroku.junction.NovelVolumeAuthor
import xyz.kemonomachi.mokuroku.novel.series.edit.Data as NovelSeriesEditData
import xyz.kemonomachi.mokuroku.novel.series.Entity as NovelSeriesEntity
import xyz.kemonomachi.mokuroku.novel.volume.create_edit.Repo
import xyz.kemonomachi.mokuroku.novel.volume.Entity as NovelVolumeEntity
import xyz.kemonomachi.mokuroku.R
import xyz.kemonomachi.mokuroku.ui.components.UpButton

private const val BASE_ROUTE = "novel/volume/edit"
private val ROUTE = "$BASE_ROUTE/{${Args.id.name}}"

internal fun route(id: Long) = "$BASE_ROUTE/$id"

internal fun NavGraphBuilder.destination(navCon: NavController) =
  composable(ROUTE, Args.id) {
    Screen(navigationIcon = { UpButton(navCon) }) {
      ViewModel(Repo(), it.getId(), navCon::popBackStack)
    }
  }

internal data class Data(
  @Embedded
  override val volume: NovelVolumeEntity,

  @Relation(
    entity = NovelSeriesEntity::class,
    parentColumn = "novel_series_id",
    entityColumn = "id"
  )
  override val series: NovelSeriesEditData,

  @Relation(
    parentColumn = "id",
    entityColumn = "id",
    associateBy = Junction(
      value = NovelVolumeAuthor::class,
      parentColumn = "novel_volume_id",
      entityColumn = "author_id"
    )
  )
  override val authors: List<CreditableEntity>
) : BaseData<NovelSeriesEntity>

@Composable
internal fun Screen(
  navigationIcon: @Composable () -> Unit,
  createViewModel: () -> BaseViewModel<NovelVolumeEntity, NovelSeriesEntity>
) {
  BaseScreen<NovelVolumeEntity, NovelSeriesEntity>(
    viewModel = viewModel(factory = ViewModelFactory(createViewModel)),
    titleId = R.string.novel_volume,
    createSeries = {
      title, workId -> NovelSeriesEntity(title = title, workId = workId)
    },
    navigationIcon = navigationIcon
  )
}
