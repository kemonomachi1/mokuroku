package xyz.kemonomachi.mokuroku.novel.volume.variant

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Entity as RoomEntity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.Insert
import androidx.room.PrimaryKey

import xyz.kemonomachi.mokuroku.base.volume.Variant

@RoomEntity(
  tableName = "novel_volume_variants",
  indices = [Index(value = ["novel_volume_id"])],
  foreignKeys = [
    ForeignKey(
      entity = xyz.kemonomachi.mokuroku.novel.volume.Entity::class,
      parentColumns = ["id"],
      childColumns = ["novel_volume_id"]
    )
  ]
)
internal data class Entity(
  @PrimaryKey(autoGenerate = true) val id: Long = 0,
  override val name: String,
  @ColumnInfo(name = "novel_volume_id") val novelVolumeId: Long
) : Variant

@Dao
internal interface DAO {
  @Insert suspend fun insert(entity: Entity): Long
}
