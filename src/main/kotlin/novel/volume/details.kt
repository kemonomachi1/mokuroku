package xyz.kemonomachi.mokuroku.novel.volume.details

import kotlin.collections.List

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.lifecycle.ViewModel as AndroidViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

import xyz.kemonomachi.mokuroku.base.composable
import xyz.kemonomachi.mokuroku.base.volume.details.*
import xyz.kemonomachi.mokuroku.creditable.details.route as creditableDetailsRoute
import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.junction.NovelVolumeAuthor
import xyz.kemonomachi.mokuroku.novel.volume.create.fromVolumeRoute as createRoute
import xyz.kemonomachi.mokuroku.novel.volume.edit.route as editRoute
import xyz.kemonomachi.mokuroku.novel.volume.variant.edit.route as variantEditRoute
import xyz.kemonomachi.mokuroku.ui.components.ScanFAB
import xyz.kemonomachi.mokuroku.ui.components.UpButton

private const val BASE_ROUTE = "novel/volume/details"
private val ROUTE = "$BASE_ROUTE/{${Args.id.name}}"

internal fun route(id: Long) = "$BASE_ROUTE/$id"

internal fun NavGraphBuilder.destination(navCon: NavController) =
  composable(ROUTE, Args.id) {
    val id = it.getId()

    Screen(
      id = id,
      toNovelVolumeEdit = { navCon.navigate(editRoute(id)) },
      toCreditableDetails = { navCon.navigate(creditableDetailsRoute(it)) },
      addVariant = { navCon.navigate(variantEditRoute(id)) },
      floatingActionButton = { ScanFAB(navCon) { createRoute(it, id) } },
      navigationIcon = { UpButton(navCon) }
    )
  }

internal data class Data(
  @Embedded
  override val volume: xyz.kemonomachi.mokuroku.novel.volume.Entity,

  @Relation(parentColumn = "novel_series_id", entityColumn = "id")
  override val series: xyz.kemonomachi.mokuroku.novel.series.Entity,

  @Relation(parentColumn = "id", entityColumn = "novel_volume_id")
  override val variants: List<xyz.kemonomachi.mokuroku.novel.volume.variant.Entity>,

  @Relation(
    parentColumn = "id",
    entityColumn = "id",
    associateBy = Junction(
      value = NovelVolumeAuthor::class,
      parentColumn = "novel_volume_id",
      entityColumn = "author_id"
    )
  )
  override val authors: List<xyz.kemonomachi.mokuroku.creditable.Entity>
) : Details

@Composable
private fun Screen(
  id: Long,
  toNovelVolumeEdit: () -> Unit,
  toCreditableDetails: (Long) -> Unit,
  addVariant: () -> Unit,
  floatingActionButton: @Composable () -> Unit,
  navigationIcon: @Composable () -> Unit
) {
  val viewModel: ViewModel = viewModel(factory = ViewModelFactory(id))
  val state by viewModel.contents.observeAsState()

  Screen(
    state = state,
    toVolumeEdit = toNovelVolumeEdit,
    toCreditableDetails = toCreditableDetails,
    addVariant = addVariant,
    floatingActionButton = floatingActionButton,
    navigationIcon = navigationIcon
  )
}

private class ViewModelFactory(
  private val id: Long
) : ViewModelProvider.Factory {

  @Suppress("UNCHECKED_CAST")
  override fun <T : AndroidViewModel> create(modelClass: Class<T>) =
    ViewModel(id) as T
}

internal class ViewModel(val id: Long) : AndroidViewModel() {
  val contents = AppDatabase.novelVolumeDAO().details(id)
}
