package xyz.kemonomachi.mokuroku.manga.volume.edit

import androidx.compose.runtime.Composable
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

import xyz.kemonomachi.mokuroku.base.composable
import xyz.kemonomachi.mokuroku.base.volume.create_edit.Screen as BaseScreen
import xyz.kemonomachi.mokuroku.base.volume.create_edit.ViewModel as BaseViewModel
import xyz.kemonomachi.mokuroku.base.volume.create_edit.ViewModelFactory
import xyz.kemonomachi.mokuroku.base.volume.edit.*
import xyz.kemonomachi.mokuroku.base.volume.edit.Data as BaseData
import xyz.kemonomachi.mokuroku.creditable.Entity as CreditableEntity
import xyz.kemonomachi.mokuroku.junction.MangaVolumeAuthor
import xyz.kemonomachi.mokuroku.manga.series.edit.Data as MangaSeriesEditData
import xyz.kemonomachi.mokuroku.manga.series.Entity as MangaSeriesEntity
import xyz.kemonomachi.mokuroku.manga.volume.create_edit.Repo
import xyz.kemonomachi.mokuroku.manga.volume.Entity as MangaVolumeEntity
import xyz.kemonomachi.mokuroku.R
import xyz.kemonomachi.mokuroku.ui.components.UpButton

private const val BASE_ROUTE = "manga/volume/edit"
private val ROUTE = "$BASE_ROUTE/{${Args.id.name}}"

internal fun route(id: Long) = "$BASE_ROUTE/$id"

internal fun NavGraphBuilder.destination(navCon: NavController) =
  composable(ROUTE, Args.id) {
    Screen(navigationIcon = { UpButton(navCon) }) {
      ViewModel(Repo(), it.getId(), navCon::popBackStack)
    }
  }

internal data class Data(
  @Embedded
  override val volume: MangaVolumeEntity,

  @Relation(
    entity = MangaSeriesEntity::class,
    parentColumn = "manga_series_id",
    entityColumn = "id"
  )
  override val series: MangaSeriesEditData,

  @Relation(
    parentColumn = "id",
    entityColumn = "id",
    associateBy = Junction(
      value = MangaVolumeAuthor::class,
      parentColumn = "manga_volume_id",
      entityColumn = "author_id"
    )
  )
  override val authors: List<CreditableEntity>
) : BaseData<MangaSeriesEntity>

@Composable
internal fun Screen(
  navigationIcon: @Composable () -> Unit,
  createViewModel: () -> BaseViewModel<MangaVolumeEntity, MangaSeriesEntity>
) {
  BaseScreen<MangaVolumeEntity, MangaSeriesEntity>(
    viewModel = viewModel(factory = ViewModelFactory(createViewModel)),
    titleId = R.string.manga_volume,
    createSeries = {
      title, workId -> MangaSeriesEntity(title = title, workId = workId)
    },
    navigationIcon = navigationIcon
  )
}
