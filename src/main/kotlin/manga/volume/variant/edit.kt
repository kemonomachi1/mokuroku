package xyz.kemonomachi.mokuroku.manga.volume.variant.edit

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel as AndroidViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder

import xyz.kemonomachi.mokuroku.base.composable
import xyz.kemonomachi.mokuroku.base.volume.variant.edit.*
import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.R
import xyz.kemonomachi.mokuroku.ui.components.DoneButton
import xyz.kemonomachi.mokuroku.ui.components.DoneTextField
import xyz.kemonomachi.mokuroku.ui.components.Scaffold
import xyz.kemonomachi.mokuroku.ui.components.UpButton

private val BASE_ROUTE = "manga/volume/variant/edit"
private val ROUTE = "$BASE_ROUTE/{${Args.id.name}}"

internal fun route(id: Long) = "$BASE_ROUTE/$id"

internal fun NavGraphBuilder.destination(navCon: NavController) =
  composable(ROUTE, Args.id) {
    Screen(
      mangaVolumeId = it.getId(),
      navigationIcon = { UpButton(navCon) },
      exit = navCon::popBackStack
    )
  }

@Composable
private fun Screen(
  mangaVolumeId: Long,
  navigationIcon: @Composable () -> Unit,
  exit: () -> Unit
) {
  val viewModel: ViewModel = viewModel(factory = ViewModelFactory(mangaVolumeId, exit))

  Scaffold(
    title = stringResource(R.string.variant),
    navigationIcon = navigationIcon,
    actions = {
      DoneButton {
        viewModel.saveAndExit()
      }
    }
  ) {
    val name by viewModel.name.observeAsState(initial = "")

    DoneTextField(
      value = name,
      onValueChange = { viewModel.name.setValue(it) },
      labelId = R.string.variant,
      onDone = {
        viewModel.saveAndExit()
      }
    )
  }
}

private class ViewModelFactory(
  private val mangaVolumeId: Long,
  private val exit: () -> Unit
) : ViewModelProvider.Factory {

  @Suppress("UNCHECKED_CAST")
  override fun <T : AndroidViewModel> create(modelClass: Class<T>) =
    ViewModel(mangaVolumeId, exit) as T
}

internal class ViewModel(
  private val mangaVolumeId: Long,
  private val exit: () -> Unit
) : AndroidViewModel() {
  val name = MutableLiveData<String>()

  fun saveAndExit() =
    viewModelScope.launch(Dispatchers.IO) {
      AppDatabase.mangaVolumeVariantDAO().insert(
        xyz.kemonomachi.mokuroku.manga.volume.variant.Entity(
          name = name.value!!,
          mangaVolumeId = mangaVolumeId
        )
      )

      withContext(Dispatchers.Main) { exit() }
    }
}
