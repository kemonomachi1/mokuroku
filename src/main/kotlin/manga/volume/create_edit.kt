package xyz.kemonomachi.mokuroku.manga.volume.create_edit

import xyz.kemonomachi.mokuroku.base.volume.create_edit.Repo as BaseRepo
import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.manga.series.Entity as MangaSeriesEntity
import xyz.kemonomachi.mokuroku.manga.volume.Entity as MangaVolumeEntity

internal class Repo : BaseRepo<MangaVolumeEntity, MangaSeriesEntity> {
  override val volumeDAO = AppDatabase.mangaVolumeDAO()
  override val seriesDAO = AppDatabase.mangaSeriesDAO()
}
