package xyz.kemonomachi.mokuroku.manga.volume

import java.time.LocalDate

import androidx.lifecycle.LiveData
import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Entity as RoomEntity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Transaction

import xyz.kemonomachi.mokuroku.base.Volume
import xyz.kemonomachi.mokuroku.base.volume.DAO as VolumeDAO
import xyz.kemonomachi.mokuroku.creditable.Entity as CreditableEntity
import xyz.kemonomachi.mokuroku.data.ISBN
import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.manga.series.Entity as MangaSeriesEntity
import xyz.kemonomachi.mokuroku.manga.series.edit.Data as MangaSeriesEditData
import xyz.kemonomachi.mokuroku.manga.volume.details.Data as DetailsData
import xyz.kemonomachi.mokuroku.manga.volume.edit.Data as EditData
import xyz.kemonomachi.mokuroku.work.Entity as WorkEntity

@RoomEntity(
  tableName = "manga_volumes",
  indices = [Index(value = ["manga_series_id"])],
  foreignKeys = [
    ForeignKey(
      entity = xyz.kemonomachi.mokuroku.manga.series.Entity::class,
      parentColumns = ["id"],
      childColumns = ["manga_series_id"]
    )
  ]
)
internal data class Entity(
  @PrimaryKey(autoGenerate = true) override val id: Long = 0,
  override val isbn: ISBN,
  override val title: String,
  override val volume: Int,
  @ColumnInfo(name = "manga_series_id") val mangaSeriesId: Long,
  @ColumnInfo(name = "publication_date") override val publicationDate: LocalDate?
) : Volume

@Dao
internal abstract class DAO : VolumeDAO<Entity, MangaSeriesEntity>() {
  @Transaction
  @Query("""
    select id,isbn,title,volume,manga_series_id,publication_date
    from manga_volumes
    where id = :id
  """)
  abstract override fun edit(id: Long): LiveData<EditData>

  @Transaction
  @Query("""
    select manga_series.id,manga_series.title,manga_series.work_id
    from manga_series
    inner join manga_volumes
    on manga_series.id = manga_volumes.manga_series_id
    where manga_volumes.id = :id
  """)
  abstract override fun editSeries(id: Long): LiveData<MangaSeriesEditData>

  @Transaction
  @Query("""
    select id,isbn,title,volume,manga_series_id,publication_date
    from manga_volumes
    where id = :id
  """)
  abstract fun details(id: Long): LiveData<DetailsData>

  @Transaction
  @Query("""
    select id,isbn,title,volume,manga_series_id,publication_date
    from manga_volumes
    where isbn = :isbn
  """)
  abstract suspend fun byISBN(isbn: ISBN): DetailsData?

  @Transaction
  override suspend fun insert(
    isbn: ISBN,
    title: String,
    volume: Int,
    publicationDate: LocalDate,
    series: MangaSeriesEntity,
    work: WorkEntity,
    authors: List<CreditableEntity>
  ) = insert(Entity(
    isbn = isbn,
    title = title,
    volume = volume,
    mangaSeriesId = AppDatabase.mangaSeriesDAO().ensure(
      series.withWork(AppDatabase.workDAO().ensure(work))
    ),
    publicationDate = publicationDate
  )).also {
    AppDatabase.mangaVolumeAuthorDAO().insert(it, authors)
  }

  @Transaction
  override suspend fun update(
    id: Long,
    isbn: ISBN,
    title: String,
    volume: Int,
    publicationDate: LocalDate,
    series: MangaSeriesEntity,
    work: WorkEntity,
    addedAuthors: List<CreditableEntity>,
    deletedAuthors: List<CreditableEntity>
  ) {
    update(Entity(
      id = id,
      isbn = isbn,
      title = title,
      volume = volume,
      mangaSeriesId = AppDatabase.mangaSeriesDAO().ensure(
        series.withWork(AppDatabase.workDAO().ensure(work))
      ),
      publicationDate = publicationDate))

    with(AppDatabase.mangaVolumeAuthorDAO()) {
      delete(id, deletedAuthors)
      insert(id, addedAuthors)
    }
  }
}
