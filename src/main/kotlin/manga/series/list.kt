package xyz.kemonomachi.mokuroku.manga.series.list

import androidx.room.ColumnInfo
import androidx.room.DatabaseView
import androidx.room.Embedded

import xyz.kemonomachi.mokuroku.base.series.ListElement

@DatabaseView(
  viewName = "MangaSeriesWithVolumeCount",
  value = """
select
  manga_series.id,
  manga_series.title,
  manga_series.work_id,
  count(manga_volumes.id) as volume_count
from manga_series
left join manga_volumes
on manga_series.id = manga_volumes.manga_series_id
group by manga_series.id
""")
internal data class Element(
  @Embedded override val series: xyz.kemonomachi.mokuroku.manga.series.Entity,
  @ColumnInfo(name = "volume_count") override val volumeCount: Int
) : ListElement
