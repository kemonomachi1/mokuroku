package xyz.kemonomachi.mokuroku.manga.series.edit

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.room.Embedded
import androidx.room.Relation

import xyz.kemonomachi.mokuroku.base.composable
import xyz.kemonomachi.mokuroku.base.series.edit.*
import xyz.kemonomachi.mokuroku.base.series.edit.Data as BaseData
import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.R
import xyz.kemonomachi.mokuroku.manga.series.Entity as MangaSeriesEntity
import xyz.kemonomachi.mokuroku.ui.components.UpButton
import xyz.kemonomachi.mokuroku.work.Entity as WorkEntity

private const val BASE_ROUTE = "manga/series/edit"
private val ROUTE = "$BASE_ROUTE/{${Args.id.name}}"

internal fun route(id: Long) = "$BASE_ROUTE/$id"

internal fun NavGraphBuilder.destination(navCon: NavController) =
  composable(ROUTE, Args.id) {
    Screen(R.string.series, { UpButton(navCon) }) {
      ViewModel(it.getId(), AppDatabase.mangaSeriesDAO(), navCon::popBackStack)
    }
  }

internal data class Data(
  @Embedded
  override val series: MangaSeriesEntity,

  @Relation(parentColumn = "work_id", entityColumn = "id")
  override val work: WorkEntity
) : BaseData<MangaSeriesEntity>
