package xyz.kemonomachi.mokuroku.manga.series

import kotlin.collections.List

import androidx.lifecycle.LiveData
import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Entity as RoomEntity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Transaction

import xyz.kemonomachi.mokuroku.base.Series
import xyz.kemonomachi.mokuroku.base.series.DAO as SeriesDAO
import xyz.kemonomachi.mokuroku.creditable.Entity as CreditableEntity
import xyz.kemonomachi.mokuroku.data.AppDatabase
import xyz.kemonomachi.mokuroku.manga.series.details.Data as DetailsData
import xyz.kemonomachi.mokuroku.manga.series.edit.Data as EditData
import xyz.kemonomachi.mokuroku.work.Entity as WorkEntity

@RoomEntity(
  tableName = "manga_series",
  indices = [Index(value = ["work_id"])],
  foreignKeys = [
    ForeignKey(
      entity = xyz.kemonomachi.mokuroku.work.Entity::class,
      parentColumns = ["id"],
      childColumns = ["work_id"]
    )
  ]
)
internal data class Entity(
  @PrimaryKey(autoGenerate = true) override val id: Long = 0,
  override val title: String,
  @ColumnInfo(name = "work_id") val workId: Long
) : Series<Entity>() {
  override fun withWork(workId: Long) = copy(workId = workId)
}

@Dao
internal abstract class DAO : SeriesDAO<Entity>() {
  @Transaction
  @Query("""
    select id,title,work_id
    from manga_series
    where id = :id
  """)
  abstract override fun edit(id: Long): LiveData<EditData>

  @Transaction
  @Query("""
    select id,title,work_id
    from manga_series
    where id = :id
  """)
  abstract fun details(id: Long): LiveData<DetailsData>

  @Query("""
    select id,title,work_id
    from manga_series
    where work_id = :workId
  """)
  abstract override fun byWork(workId: Long): LiveData<List<Entity>>

  @Query("""
    select distinct creditables.id,creditables.name
    from creditables
    inner join manga_volume_authors
    on creditables.id = manga_volume_authors.author_id
    inner join manga_volumes
    on manga_volume_authors.manga_volume_id = manga_volumes.id
    where manga_volumes.manga_series_id = :id
  """)
  abstract override fun authors(id: Long): LiveData<List<CreditableEntity>>

  @Transaction
  override suspend fun update(id: Long, title: String, work: WorkEntity) =
    update(Entity(
      id = id,
      title = title,
      workId = AppDatabase.workDAO().ensure(work)
    ))
}
