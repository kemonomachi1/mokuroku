# Mokuroku

Mokuroku is a personal library indexing app for Japanese manga and novels.

## What

Basically a glorified database of manga and novel volumes, grouped by series and work (series belonging to the same "universe").
Each volume is registered with ISBN, title, volume number, publication date, and authors.
Also includes [ZXing Android Embedded](https://github.com/journeyapps/zxing-android-embedded) for scanning ISBN barcodes, both for registering new volumes and for looking up already registered ones.

## Why

Once my collection reached 500 volumes or so, I realized that I had problems keeping track of everything.
I could have created a spreadsheet, but a custom app is way more fun.
Also a good excuse to learn Kotlin.

## Warning

Mokuroku is a one-developer project with a target audience of one (me).
In general, it does not follow best practices:
- No tests
- Pretty much no comments or other documentation
- Unorthodox naming conventions - Many functions/classes have identical names and are differentiated by package name only
- Using the latest and greatest dependencies, even beta and alpha versions
- Deliberately crashing on invalid user input
- UI language is Japanese, with no English fallback

## About the Name

目録 (もくろく, mokuroku) means "index" in Japanese
